<?php

return [
    'new'                   => 'Thêm mới người dùng',
    'edit'                  => 'Sửa thông tin người dùng',
    'name'                  => 'Tên họ',
    'username'              => 'Tên đăng nhập',
    'change_password'       => 'Thay đổi mật khẩu',
    'active'                => 'Hoạt động',
    'password'              => 'Mật khẩu',
    'password_confirmation' => 'Nhập lại mật khẩu',
    'lang'                  => 'Ngôn ngữ hiện thị',
    'avatar'                => 'Ảnh avatar',
    'chose_avatar'          => 'Chọn ảnh avatar',
    'about'                 => 'Giới thiệu',
    'phone'                 => 'Số điện thoại',
    'email'                 => 'Địa chỉ Email',
    'birthday'              => 'Ngày sinh',
    'gender'                => 'Giới tính',
    'gender0'               => 'Nữ',
    'gender1'               => 'Nam',
    'address'               => 'Địa chỉ',
    'last_login'            => 'Lần cuối đăng nhập',
];
