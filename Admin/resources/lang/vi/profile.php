<?php

return [
    'new'                   => 'Thêm mới admin',
    'edit'                  => 'Sửa thông tin admin',
    'name'                  => 'Tên họ',
    'username'              => 'Tên đăng nhập',
    'change_password'       => 'Thay đổi mật khẩu',
    'active'                => 'Hoạt động',
    'password'              => 'Mật khẩu',
    'password_confirmation' => 'Nhập lại mật khẩu',
    'lang'                  => 'Ngôn ngữ hiện thị',
    'avatar'                => 'Ảnh avatar',
    'chose_avatar'          => 'Chọn ảnh avatar',
    'roles'                 => 'Nhóm quyền',
    'permissions'           => 'Danh sách quyền',
    'last_login'            => 'Lần cuối đăng nhập',
];
