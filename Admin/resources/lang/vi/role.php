<?php

return [
    'name'              => 'Tên vai trò',
    'guard_name'        => 'Guard',
    'permissions'       => 'Quyền hạn',
    'count_permissions' => 'Số quyền hạn',
    'count_admin_use'   => 'Số admin dùng',
];
