@extends('admin::layouts.index')

@section('body')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Danh sách admin</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-3">
                            <input name="search" placeholder="Tên" type="text" value="{{getGridInputOld('search')}}"
                                   class="form-control change_grid">
                        </div>
                        <div class="col-sm-2">
                            {!! Form::select('role_id', $roles_pluck->prepend('Tất cả vai trò',''), getGridInputOld('role_id'), ['class' => 'form-control chosen-select change_grid']) !!}
                        </div>
                        <div class="col-sm-2">
                            <select name="active" data-placeholder="Trạng thái" class="chosen-select change_grid">
                                <option></option>
                                <option value="1">Hoạt động</option>
                                <option value="0">Ngừng hoạt động</option>
                            </select>
                        </div>
                        <div class="mb-1 col-6 col-md-3 col-xl-2">
                            @include('admin::common._button_search_table')
                        </div>
                    </div>

                    <div class="noi_dung_table">
                        <div class="gridTable"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link href="{{asset_admin('plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">
@endpush

@push('js')
    <script src="{{asset_admin('plugins/chosen/chosen.jquery.js')}}"></script>
    <script>
        $(function () {
            $('.chosen-select').chosen({
                width: "100%",
                allow_single_deselect: true
            });
        })
    </script>
@endpush
