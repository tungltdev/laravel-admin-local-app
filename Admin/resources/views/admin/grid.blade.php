<div class="table-responsive">
    <table class="data-table table table-striped table-bordered">
        <thead>
        <tr>
            <th class="table_stt sort" data-sort="id">ID</th>
            <th class="sort" data-sort="username">{{__("admin::$type.username")}}</th>
            <th class="sort" data-sort="name">{{__("admin::$type.name")}}</th>
            <th>{{__("admin::$type.roles")}}</th>
            <th>{{__("admin::$type.permissions")}}</th>
            <th>{{__("admin::$type.lang")}}</th>
            <th class="sort" data-sort="last_login">{{__("admin::$type.last_login")}}</th>
            @include('admin::common.th_action_table')
        </tr>
        </thead>
        <tbody>
        @if(count($docs))
            @foreach($docs as $key => $doc)
                <tr>
                    <td class="table_id"> {{$doc->id}} </td>
                    <td>{{$doc->username}}</td>
                    <td>{{$doc->name}}</td>
                    <td title="{{$doc->roles()->pluck('name')}}">{{$doc->roles()->count()}}</td>
                    <td title="{{$doc->permissions()->pluck('name')}}">{{$doc->permissions()->count()}}</td>
                    <td>{{$doc->lang}}</td>
                    <td>{{$doc->last_login?$doc->last_login->format($constant::FORMAT_DATE_TIME):''}}</td>
                    @include('admin::common.th_edit_delete_table')
                </tr>
            @endforeach
        @else
            <tr>
                <td class="no-data"></td>
            </tr>
        @endif
        </tbody>
    </table>
</div>

@include('admin::common.gridFooter')
