<div class="row">
    <div class="col-12">
        <div class="ibox">
            <div class="ibox-title">
                <h5>Thêm mới Admin</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="form-group @error('name') has-error @enderror">
                                {!! Form::label('name', __("admin::$type.name"), array('class' => ' required')) !!}
                                {!! Form::text('name', null, array('class' => 'form-control',
                                'placeholder'=> __("admin::$type.name"), 'required' => true,
                                'minlength'=>$constant::VALIDATE['name_min_len'],
                                'maxlength'=>$constant::VALIDATE['name_max_len'])) !!}
                                <span class="help-block">{{ $errors->first('name', ':message') }}</span>
                            </div>
                        </div>

                        <div class="col-xs-12 col-md-6">
                            <div class="form-group @error('username') has-error @enderror">
                                {!! Form::label('username', __("admin::$type.username"), array('class' => ' required')) !!}
                                {!! Form::text('username', null, array('class' => 'form-control',
                                'placeholder'=> __("admin::$type.username"), 'required' => true,
                                'minlength'=>$constant::VALIDATE['username_min_len'],
                                'maxlength'=>$constant::VALIDATE['username_max_len'])) !!}
                                <span class="help-block">{{ $errors->first('username', ':message') }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="form-group @error('lang') has-error @enderror">
                                {!! Form::label('lang', __("admin::$type.lang"),array('class' => ' required')) !!}
                                {!! Form::select('lang', $constant::LANG,null, array('class' => 'form-control')) !!}
                                <span class="help-block">{{ $errors->first('lang', ':message') }}</span>
                            </div>
                        </div>
                    </div>

                    @if (isset($doc))
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <div class="i-checks">
                                        <label for="remember">
                                            <div class="icheckbox_square-green">
                                                <input name="show_pass" id="show_pass" type="checkbox">
                                            </div>
                                            {{ __("admin::$type.change_password") }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <div class="i-checks">
                                        <label for="remember">
                                            <div class="icheckbox_square-green">
                                                {!! Form::checkbox('active', null,@$doc->active) !!}
                                            </div>
                                            {{ __("admin::$type.active") }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group @error('password') has-error @enderror password_field">
                                {!! Form::label('password', __("admin::$type.password"),array('class'=>empty($doc)?"required":'')) !!}
                                {!! Form::password('password', array('class' => 'form-control',empty($doc)?"required":'',
                                'placeholder'=> __("admin::$type.password"), 'data-bv-identical'=>true,
                                'data-bv-identical-field'=>'confirmPassword',
                                'minlength'=>$constant::VALIDATE['password_min_len'],
                                'maxlength'=>$constant::VALIDATE['password_max_len']
                                )) !!}
                                <span class="help-block">{{ $errors->first('password', ':message') }}</span>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="form-group @error('password_confirmation') has-error @enderror password_field">
                                {!! Form::label('password_confirmation', __("admin::$type.password_confirmation"),array('class'=>empty($doc)?"required":'')) !!}
                                {!! Form::password('password_confirmation', array('class' => 'form-control',empty($doc)?"required":'',
                                'placeholder'=> __("admin::$type.password_confirmation"),
                                'data-bv-identical'=>true,
                                'data-bv-identical-field'=>"password"
                                )) !!}
                                <span class="help-block">{{ $errors->first('password_confirmation', ':message') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-4 ">
                            <div class="img_upload">
                                {!! Form::label('avatar', __("admin::$type.avatar")) !!}
                                <img class="img_upload_file" src="{{getImageStorage(@$doc->image)}}"/>
                                <a href="javascript:void(0)" class="btn btn-default">
                                    {{ __("admin::$type.chose_avatar") }}
                                    <input type="file" onchange="imagePreview(this);"
                                           name="img_file" class="input_file_img"/>
                                </a>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="form-group @error('role_ids') has-error @enderror">
                                {!! Form::label('role_ids', __("admin::$type.roles"), array('class' => 'col-form-label required')) !!}
                                {!! Form::select('role_ids[]', $roles_pluck, @$doc->roles, ['class' => 'form-control dual_select','multiple'=>true]) !!}
                                <span class="help-block">{{ $errors->first('role_ids', ':message') }}</span>
                            </div>
                        </div>

                        <div class="col-12 col-md-6">
                            <div class="form-group @error('role_ids') has-error @enderror">
                                {!! Form::label('permission_ids', __("admin::$type.permissions"), array('class' => 'col-form-label required')) !!}
                                {!! Form::select('permission_ids[]', $permissions_pluck, @$doc->permissions, ['class' => 'form-control dual_select','multiple'=>true]) !!}
                                <span class="help-block">{{ $errors->first('permission_ids', ':message') }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="ibox-footer">
                @include('admin::common._button_submit')
            </div>
        </div>
    </div>
</div>

@push('css')
    <link href="{{asset_admin('plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{asset_admin('css/glyphicons.css')}}" rel="stylesheet">
    <link href="{{asset_admin('plugins/dualListbox/bootstrap-duallistbox.min.css')}}" rel="stylesheet">
@endpush

@push('js')
    <!-- iCheck -->
    <script src="{{asset_admin('plugins/iCheck/icheck.min.js')}}"></script>
    <script src="{{asset_admin('plugins/dualListbox/jquery.bootstrap-duallistbox.js')}}"></script>

    <script>
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        $('.dual_select').bootstrapDualListbox({
            selectorMinimalHeight: 160,
            filterPlaceHolder: 'Tìm kiếm',
            infoText: 'Hiện thị {0}',
            infoTextFiltered: '<span class="label label-warning">Kết quả lọc</span> {0} đến {1}',
            infoTextEmpty: 'Trống',
            filterTextClear: 'Xóa lọc',
            preserveSelectionOnMove: false,
            moveOnSelect: false,
        });

        @if (isset($doc))
        if ($(".password_field").hasClass('has-error')) {
            $("#show_pass").iCheck('check');
            $(".password_field").show();
        } else {
            $(".password_field").hide();
        }
        $("#show_pass").on('ifChanged', function () {
            if ($("#show_pass").is(':checked')) {
                $(".password_field").show();
            } else {
                $(".password_field").hide();
            }
        });
        @endif
    </script>
@endpush
