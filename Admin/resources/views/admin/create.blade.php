@extends('admin.layouts.app')

@section('dashboard-header')
    <div class="col-lg-10">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.html">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a>Tables</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Static Tables</strong>
            </li>
        </ol>
    </div>
@stop

@section('content')

    @if (isset($doc))
        {!! Form::model($doc, array('url' => $link_edit, 'method' => 'put', 'files'=> true,'class'=>'f_validate')) !!}
    @else
        {!! Form::open(array('url' => $link_add, 'method' => 'post', 'files'=> true,'class'=>'f_validate')) !!}
    @endif

    @includeFirst(["admin/$type/form"])

    {!! Form::close() !!}
@stop
