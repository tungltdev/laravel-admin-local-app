@extends('admin::layouts.app')
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Thông tin cá nhân</h2>
        </div>
    </div>

    <div class="wrapper wrapper-content  animated fadeInRight">
        <div class="row animated fadeInRight">
            <div class="col-md-4">
                <div class="ibox float-e-margins" id="profiletrai">
                    <div>
                        <div class="ibox-content no-padding border-left-right">
                            <img alt="image" class="img-fluid rounded-circle d-block"
                                 src="{{getImageStorage(@$admin->image)}}">
                            <span>{{$admin->name}}</span>
                        </div>
                        <div class="ibox-content profile-content thongtinthemprofile">
                            <span class="list-group-item active">
                                <span class="fa fa-bar-chart-o"></span>
                                Thông tin cá nhân
                            </span>
                            <span class="list-group-item">
                                <span class="fa fa-heart"></span> Tài khoản
                                <span class="badge badge-primary">{{$admin->username}}</span>
                            </span>
                            <span class="list-group-item">
                                <span class="fa fa-phone"></span> Ngôn ngữ
                                <span class="badge badge-warning">{{$admin->lang}}</span>
                            </span>
                            <span class="list-group-item">
                                <span class="fa fa-pagelines"></span> Ngày tạo
                                <span class="badge badge-inverse">{{$admin->created_at->format(\App\Admin\Constants\Constants::FORMAT_DATE_TIME)}}</span>
                            </span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Sửa thông tin cá nhân</h5>
                    </div>
                    <div class="ibox-content">
                        {!! Form::model($doc, array('url' => route_admin('updateProfile'), 'method' => 'post','files'=>true,'class'=>'f_validate')) !!}

                        <div class="row">
                            <div class="col-12 col-md-4">
                                <div class="img_upload">
                                    {!! Form::label('avatar', __("admin::$type.avatar")) !!}
                                    <img class="img_upload_file" src="{{getImageStorage(@$admin->image)}}"/>
                                    <a href="javascript:void(0)" class="btn btn-default">
                                        {{ __("admin::$type.chose_avatar") }}
                                        <input type="file" onchange="imagePreview(this);"
                                               name="img_file" class="input_file_img"/>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="form-group @error('name') has-error @enderror">
                                    {!! Form::label('name', __("admin::$type.name"), array('class' => ' required')) !!}
                                    {!! Form::text('name', null, array('class' => 'form-control',
                                    'placeholder'=> __("admin::$type.name"),
                                    'required' => true,
                                    'minlength'=>\App\Admin\Constants\AdminConstant::VALIDATE['name_min_len'],
                                    'maxlength'=>\App\Admin\Constants\AdminConstant::VALIDATE['name_max_len'])) !!}
                                    <span class="help-block">{{ $errors->first('name', ':message') }}</span>
                                </div>
                            </div>

                            <div class="col-xs-12 col-md-6">
                                <div class="form-group @error('username') has-error @enderror">
                                    {!! Form::label('username', __("admin::$type.username"), array('class' => ' required')) !!}
                                    {!! Form::text('username', null, array('class' => 'form-control',
                                    'placeholder'=> __("admin::$type.username"),
                                    'required' => true,
                                    'minlength'=>\App\Admin\Constants\AdminConstant::VALIDATE['username_min_len'],
                                    'maxlength'=>\App\Admin\Constants\AdminConstant::VALIDATE['username_max_len'])) !!}
                                    <span class="help-block">{{ $errors->first('username', ':message') }}</span>
                                </div>
                            </div>
                        </div>

                        @if (isset($doc))
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="form-group @error('lang') has-error @enderror">
                                        {!! Form::label('lang', __("admin::$type.lang"),array('class' => ' required')) !!}
                                        {!! Form::select('lang', \App\Admin\Constants\AdminConstant::LANG,null, array('class' => 'form-control')) !!}
                                        <span class="help-block">{{ $errors->first('lang', ':message') }}</span>
                                    </div>
                                </div>

                                <div class="col-xs-12 col-md-6">
                                    <div class="form-group">
                                        <div class="i-checks">
                                            <label for="remember" style="margin-top: 28px;">
                                                <div class="icheckbox_square-green">
                                                    <input name="show_pass" id="show_pass" type="checkbox">
                                                </div>
                                                {{ __("admin::$type.change_password") }}
                                            </label>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        @endif

                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group @error('password') has-error @enderror password_field">
                                    {!! Form::label('password', __("admin::$type.password"),array('class'=>empty($doc)?"required":'')) !!}
                                    {!! Form::password('password', array('class' => 'form-control',empty($doc)?"required":'',
                                    'placeholder'=> __("admin::$type.password"),
                                    'data-bv-identical'=>true,
                                    'data-bv-identical-field'=>'confirmPassword',
                                    'minlength'=>\App\Admin\Constants\AdminConstant::VALIDATE['password_min_len'],
                                    'maxlength'=>\App\Admin\Constants\AdminConstant::VALIDATE['password_max_len']
                                    )) !!}
                                    <span class="help-block">{{ $errors->first('password', ':message') }}</span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <div
                                        class="form-group @error('password_confirmation') has-error @enderror password_field">
                                    {!! Form::label('password_confirmation', __("admin::$type.password_confirmation"),array('class'=>empty($doc)?"required":'')) !!}
                                    {!! Form::password('password_confirmation', array('class' => 'form-control',empty($doc)?"required":'',
                                    'placeholder'=> __("admin::$type.password_confirmation"),
                                    'data-bv-identical'=>true,
                                    'data-bv-identical-field'=>"password"
                                    )) !!}
                                    <span class="help-block">{{ $errors->first('password_confirmation', ':message') }}</span>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-sm btn-primary" type="submit"> Lưu thông tin</button>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('css')
    <style>
        .thongtinthemprofile {
            padding: 0 !important;
        }

        .thongtinthemprofile span.list-group-item {
            cursor: pointer;
        }

        .thongtinthemprofile .list-group-item {
            border-radius: 0 !important;
        }

        #profiletrai {
            border: solid 1px burlywood;
        }

        #profiletrai .border-left-right img {
            border: 3px solid #FFF;
            margin: 10px auto;
            width: 150px;
            height: 150px;
        }

        #profiletrai .border-left-right span {
            display: block;
            text-align: center;
            font-weight: bold;
            font-size: 20px;
            color: #5aff1d;
            text-shadow: -2px -1px 0 #a47968;
        }
    </style>
@endpush

@push('css')
    <link href="{{asset_admin('plugins/iCheck/custom.css')}}" rel="stylesheet">
@endpush

@push('js')
    <!-- iCheck -->
    <script src="{{asset_admin('plugins/iCheck/icheck.min.js')}}"></script>

    <script>
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        @if (isset($doc))
        if ($(".password_field").hasClass('has-error')) {
            $("#show_pass").iCheck('check');
            $(".password_field").show();
        } else {
            $(".password_field").hide();
        }
        $("#show_pass").on('ifChanged', function () {
            if ($("#show_pass").is(':checked')) {
                $(".password_field").show();
            } else {
                $(".password_field").hide();
            }
        });
        @endif
    </script>
@endpush
