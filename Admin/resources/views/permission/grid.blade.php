<div class="table-responsive">
    <table class="data-table table table-striped table-bordered">
        <thead>
        <tr>
            <th class="table_stt sort" data-sort="id">ID</th>
            <th class="sort" data-sort="name">{{__("admin::$type.name")}}</th>
            <th>{{__("admin::$type.roles")}}</th>
            <th class="sort" data-sort="notes">{{__("admin::$type.notes")}}</th>
            <th class="sort" data-sort="created_at">Ngày tạo</th>
            <th style="width: 50px">Action</th>
        </tr>
        </thead>
        <tbody>
        @if(count($docs))
            @foreach($docs as $key => $doc)
                <tr>
                    <td class="table_id"> {{$doc->id}} </td>
                    <td>{{$doc->name}}</td>
                    <td>{{@$doc->roles->pluck('name')}}</td>
                    <td>{{$doc->notes}}</td>
                    <td>{{$doc->created_at->format($constant::FORMAT_DATE_TIME)}}</td>
                    <td class="table_action text-center">
                        @can($type."_edit")
                            <a href="{{ route_admin("$type.edit",@$doc->id) }}" title="{{__('table.edit')}}">
                                <i class="fa fa-fw fa-pencil text-warning"></i>
                            </a>
                        @endcan
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td class="no-data"></td>
            </tr>
        @endif
        </tbody>
    </table>
</div>

@include('admin::common.gridFooter')
