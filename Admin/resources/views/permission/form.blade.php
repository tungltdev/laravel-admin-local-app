<div class="row">
    <div class="col-12">
        <div class="ibox">
            <div class="ibox-title">
                <h5>Sửa quyền</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content">
                <div class="panel-body">
                    <div class="row @error('notes') is-invalid @enderror">
                        <div class="col-12">
                            {!! Form::text('notes', null, array('class' => 'form-control','placeholder'=> __("$type.notes"))) !!}
                            <span class="help-block">{{ $errors->first('notes', ':message') }}</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="ibox-footer">
                @include('admin::common._button_submit')
            </div>
        </div>
    </div>
</div>
