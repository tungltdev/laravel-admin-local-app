@extends('admin::layouts.index')

@section('body')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Danh sách quyền</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-2">
                            <input type="text" name="search" value="{{getGridInputOld('search')}}"
                                   class="form-control change_grid" placeholder="Tên quyền">
                        </div>
                        <div class="mb-1 col-6 col-md-3 col-xl-2">
                            @include('admin::common._button_search_table')
                        </div>
                    </div>
                    <div class="noi_dung_table">
                        <div class="gridTable"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
