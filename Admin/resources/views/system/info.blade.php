@extends('admin::layouts.master')
@section('title', "Thông tin hệ thống")
@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-12">
            <h2>Thông tin hệ thống</h2>
        </div>
    </div>

    <div class="row mt-4">
        <div class="col-12">
            <a href="{{route_admin('system.refresh.permission')}}">
                <button class="btn btn-danger dim" type="button">
                    Cập nhập quyền
                </button>
            </a>
            @can('system_fixStorageLink')
                <a href="{{route_admin('system.fixStorageLink')}}">
                    <button class="btn btn-danger dim" type="button">
                        Fix storage Link
                    </button>
                </a>
            @endcan
            @can('system_xoaCacheConfig')
                <a href="{{route_admin('system.xoaCacheConfig')}}">
                    <button class="btn btn-danger dim" type="button">
                        Xóa cache config
                    </button>
                </a>
            @endcan
            @can('system_xoaCacheView')
                <a href="{{route_admin('system.xoaCacheView')}}">
                    <button class="btn btn-danger dim" type="button">
                        Xóa cache view
                    </button>
                </a>
            @endcan
            @can('system_xoaCacheSessions')
                <a href="{{route_admin('system.xoaCacheSessions')}}">
                    <button class="btn btn-danger dim" type="button">
                        Xóa sessions
                    </button>
                </a>
            @endcan
        </div>
    </div>

    <div class="row mt-4">
        <div class="col-12 col-sm-6">
            <div class="ibox ibox-content">
                <div class="widget-title">
                    <h4>
                        <span>Cmd sys</span>
                    </h4>
                </div>
                <form method="post" action="{{route_admin('system.run_cmd')}}">
                    @csrf
                    <input type="text" class="form-control" name="cmd">
                    <input type="hidden" class="form-control" name="type" value="cmd">
                    <button type="submit" class="btn btn-success mt-1">Run</button>
                </form>
                <textarea class="form-control mt-1" rows="6"
                          placeholder="Kết quả chạy command">{{@passthru(@$passthru_cmd)}}</textarea>
            </div>
        </div>
        <div class="col-12 col-sm-6">
            <div class="ibox ibox-content">
                <div class="widget-title">
                    <h4>
                        <span>Artisan laravel</span>
                    </h4>
                </div>
                <form method="post" action="{{route_admin('system.run_cmd')}}">
                    @csrf
                    <input type="text" class="form-control" name="cmd">
                    <input type="hidden" class="form-control" name="type" value="artisan">
                    <button type="submit" class="btn btn-success mt-1">Run</button>
                </form>
                <textarea class="form-control mt-1" rows="6"
                          placeholder="Kết quả chạy artisan">{{@$artisan_cmd}}</textarea>
            </div>
        </div>
    </div>

    <div class="row mt-4">
        <div class="col-sm-6">
            <div class="ibox ibox-content">
                <div class="widget-title">
                    <h4>
                        <span>System Environment</span>
                    </h4>
                </div>
                <ul class="list-group">
                    <li class="list-group-item">Framework Version: {{app()->version()}}</li>
                    <li class="list-group-item">App Base Domain: {{env('APP_BASE_DOMAIN')}}</li>
                    <li class="list-group-item">App URL: {{config('app.url')}}</li>
                    <li class="list-group-item">App Locale: {{config('app.locale')}}</li>
                    <li class="list-group-item">Timezone: {{config('app.timezone')}}</li>
                    <li class="list-group-item">Storage
                        chmod: {{$data_more['chmod_storage_path']}} {{$data_more['perms_storage_path']}}</li>
                    <li class="list-group-item">Public
                        chmod: {{$data_more['chmod_public_path']}} {{$data_more['perms_public_path']}}</li>

                </ul>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="ibox ibox-content">
                <div class="widget-title">
                    <h4>
                        <span>Server Environment</span>
                    </h4>
                </div>
                <ul class="list-group">
                    <li class="list-group-item">App ENV: {{config('app.env')}}</li>
                    <li class="list-group-item">PHP Version: {{phpversion()}}</li>
                    <li class="list-group-item">Server Software: {{@$_SERVER['SERVER_SOFTWARE']}}</li>
                    <li class="list-group-item">Debug
                        Mode: {!! config('app.debug')?'<i class="fa fa-check"></i>':'<i class="fa fa-times"></i>' !!} </li>
                    <li class="list-group-item">Database: {{config('database.default')}}</li>
                    <li class="list-group-item">Cache Driver: {{config('cache.default')}}</li>
                    <li class="list-group-item">Session Driver: {{config('session.driver')}}</li>
                    <li class="list-group-item">Session lifetime: {{config('session.lifetime')}}</li>
                    <li class="list-group-item">Queue Connection: {{config('queue.default')}}</li>
                    <li class="list-group-item">Broadcast driver: {{config('broadcasting.default')}}</li>
                    <li class="list-group-item">Uname -a: {{@substr(@php_uname(),0,120)}} </li>
                    <li class="list-group-item">Safe Mode: {{@getCfgVar('safe_mode')?'ON':'OFF'}} </li>
                </ul>
            </div>
        </div>

    </div>


    @if($cp)
        <div class="row mt-2">
            <div class="col-12">
                <div class="ibox">
                    <a href="{{route_admin('system.refresh.cache','file.composer.lock')}}">
                        <button class="btn btn-danger dim" type="button">
                            Lấy dữ liệu mới nhất
                        </button>
                    </a>

                    <div class="ibox-title">
                        <h5>Cài đặt gói và phiên bản hệ thống</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>

                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table id="data-table" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th class="table_stt">Tên gói ({{@count($cp['packages'])}})</th>
                                    <th width="100">Phiên bản</th>
                                    <th>Mã nguồn</th>
                                    <th>Mô tả</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($cp['packages'] as $package)
                                    <tr>
                                        <td class="table_id"> {{$package['name']}}</td>
                                        <td> {{$package['version']}}</td>
                                        <td> {{@$package['source']['url']}}</td>
                                        <td> {{@$package['description']}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Cài đặt gói và phiên bản hệ thống dev</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>

                    <div class="ibox-content inspinia-timeline">
                        <div class="table-responsive">
                            <table id="data-table" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th class="table_stt">Tên gói ({{@count($cp['packages-dev'])}})</th>
                                    <th width="100">Phiên bản</th>
                                    <th>Mã nguồn</th>
                                    <th>Mô tả</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($cp['packages-dev'] as $package)
                                    <tr>
                                        <td class="table_id"> {{$package['name']}}</td>
                                        <td> {{$package['version']}}</td>
                                        <td> {{@$package['source']['url']}}</td>
                                        <td> {{@$package['description']}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    @endif
@stop
