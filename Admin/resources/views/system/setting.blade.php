@extends('admin::layouts.master')
@section('title', "Cài đặt hệ thống")
@section('content')
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-12">
            <h2>Cài đặt hệ thống</h2>
        </div>
    </div>

    <div class="row mt-4">
        <div class="col-lg-12">
            <div class="tabs-container">
                <ul class="nav nav-tabs">
                    <li><a class="nav-link active" data-toggle="tab" href="#tab-2"> Hệ thống</a></li>
                </ul>
                <div class="tab-content">
                    <div id="tab-2" class="tab-pane active">
                        <div class="panel-body">
                            {!! Form::open(array('url' => route_admin('system.saveContentEnvFile'), 'method' => 'post')) !!}
                            <fieldset>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">ENV FILE:</label>
                                    <div class="col-sm-10">
                                        <textarea name="content_env" class="form-control" cols="30"
                                                  rows="50">{{@file_get_contents(base_path('.env'))}}</textarea>
                                        <br>
                                        <button type="submit" class="btn btn-success">
                                            <i class="fa fa-check-square-o"></i> {{__('form.submit')}}
                                        </button>
                                    </div>
                                </div>
                            </fieldset>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
