@extends('admin::layouts.master')

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-12">
            <h2>Trang tổng quan</h2>
        </div>
    </div>

    <div class="row mt-4">
        <div class="col-12">
            <a href="{{route_admin('system.refresh.cache','admin.dashboard.dashboard')}}">
                <button class="btn btn-success dim" type="button">
                    Lấy dữ liệu mới nhất
                </button>
            </a>
        </div>
    </div>

    <div class="row thong_ke_tong">
        <div class="col-12 col-md-6 col-lg-3 col-xl-3">
            <div class="ibox-content card-thong-ke">
                <i class="fa fa-shopping-cart float-right"></i>
                <h5 class="text-white text-uppercase">Tổng số người dùng</h5>
                <h2>{{@number_format(@$data['user_count'])}}</h2>
            </div>
        </div>

        <div class="col-12 col-md-6 col-lg-3 col-xl-3">
            <div class="ibox-content card-thong-ke">
                <i class="fa fa-shopping-cart float-right"></i>
                <h5 class="text-white text-uppercase">Tổng admin</h5>
                <h2>{{@number_format(@$data['admin_count'])}}</h2>
            </div>
        </div>

        <div class="col-12 col-md-6 col-lg-3 col-xl-3">
            <div class="ibox-content card-thong-ke">
                <i class="fa fa-shopping-cart float-right"></i>
                <h5 class="text-white text-uppercase">Tổng bài đăng</h5>
                <h2>{{@number_format(@$data['post_count'])}}</h2>
            </div>
        </div>

        <div class="col-12 col-md-6 col-lg-3 col-xl-3">
            <div class="ibox-content card-thong-ke">
                <i class="fa fa-shopping-cart float-right"></i>
                <h5 class="text-white text-uppercase">Tổng số danh mục</h5>
                <h2>{{@number_format(@$data['category_count'])}}</h2>
            </div>
        </div>

        <div class="col-12 col-md-6 col-lg-3 col-xl-3">
            <div class="ibox-content card-thong-ke">
                <i class="fa fa-shopping-cart float-right"></i>
                <h5 class="text-white text-uppercase">Tổng Vai trò</h5>
                <h2>{{@number_format(@$data['role_count'])}}</h2>
            </div>
        </div>

        <div class="col-12 col-md-6 col-lg-3 col-xl-3">
            <div class="ibox-content card-thong-ke">
                <i class="fa fa-shopping-cart float-right"></i>
                <h5 class="text-white text-uppercase">Tổng quyền hạn</h5>
                <h2>{{@number_format(@$data['permission_count'])}}</h2>
            </div>
        </div>
    </div>
    <hr>

    <div class="row mb-4">
        <div class="col-md-2 col-xs-12">
            <h4 class="text-right">{{ __('dashboard.date_range') }}</h4>
        </div>
        <div class="col-md-10 col-xs-12">
            <div id="reportrange"
                 style="float:left; background: #fff; cursor: pointer; margin:0 auto; padding: 5px 10px; border: 1px solid #ccc; min-width: 320px">
                <i class="fa fa-calendar"></i>&nbsp;
                <span></span> <b class="caret"></b>
            </div>
            <input type="hidden" id="start-date" value="{{@$data['start']}}">
            <input type="hidden" id="end-date" value="{{@$data['end']}}">
        </div>
    </div>

    <div class="row mb-4">
        <div class="col-md-8">
            <div class="box1" style="min-height: 400px;">
                <div class="float-left">
                    <h4>{{__('dashboard.user')}} và {{__('dashboard.admin')}}</h4>
                </div>
                <div class="float-right">
                    <ul class="date-filter" id="account-filter">
                        <li class="active">
                            <a href="javascript:void(0)" data-time="day"
                               data-type="account">{{ __('dashboard.day') }}</a></li>
                        <li><a href="javascript:void(0)" data-time="week"
                               data-type="account">{{ __('dashboard.week') }}</a></li>
                        <li><a href="javascript:void(0)" data-time="month"
                               data-type="account">{{ __('dashboard.month') }}</a></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div id='account'></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="box1">
                <h4>{{__('dashboard.payment')}}</h4>
                <div id="payment"></div>

            </div>
            {{--<div class="box1 table">--}}
            {{--<h4 style="padding-left: 15px;padding-top: 15px;">{{ __('dashboard.top_product') }}</h4>--}}
            {{--{!! Form::open(array('url' => $url_current.'/grid', 'method' => 'post', 'id'=> 'gridForm','role'=>'form')) !!}--}}
            {{--<div class="noidungtable">--}}
            {{--<div id="gridTable"></div>--}}
            {{--                    @include('BE.common.gridLimit')--}}
            {{--</div>--}}
            {{--{!! Form::close() !!}--}}
            {{--</div>--}}
        </div>
    </div>

    <div class="row mb-4">
        <div class="col-12">
            <div class="box1" style="min-height: 400px;">
                <div class="float-left">
                    <h4>{{__('dashboard.post')}}</h4>
                </div>
                <div class="float-right">
                    <ul class="date-filter" id="post-filter">
                        <li class="active">
                            <a href="javascript:void(0)" data-time="day"
                               data-type="post">{{ __('dashboard.day') }}</a></li>
                        <li><a href="javascript:void(0)" data-time="week"
                               data-type="post">{{ __('dashboard.week') }}</a></li>
                        <li><a href="javascript:void(0)" data-time="month"
                               data-type="post">{{ __('dashboard.month') }}</a></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <div id='post'></div>
            </div>
        </div>
    </div>

@endsection

@push('css')
    <style>
        .thong_ke_tong div {
            margin-top: 10px;
        }

        .card-thong-ke {
            background-color: #009444;
            color: white;
        }

        .mar-20 {
            margin-top: 20px;
        }

        .DTTTFooter {
            display: none;
        }

        #gridTable .DTTTFooter {
            display: block;
        }

        .box1 {
            margin-bottom: 15px;
            padding: 10px 15px;
            background-color: #fff;
        }

        .box1.table {
            padding: 0;
        }

        .box1.table table {
            margin-bottom: 0;
        }

        .date-filter li {
            display: inline-block;
        }

        .date-filter li a {
            padding: 10px;
            background: #fbfcfc;
            border: 1px solid #d9d9d9;
            display: inline-block;
            color: #3e3d3e;
            font-weight: bold;
        }

        .date-filter li.active a, .date-filter li a:hover {
            background: #e4e4e4;
        }
    </style>

    <link href="{{ asset_admin('plugins/c3/c3.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset_admin('plugins/daterangepicker/daterangepicker-bs3.css')}}" rel="stylesheet" type="text/css"/>
@endpush

@push('js')

    <script src="{{ asset_admin('plugins/d3/d3.v3.min.js') }}"></script>
    <script src="{{ asset_admin('plugins/d3/d3.min.js') }}"></script>
    <script src="{{ asset_admin('plugins/c3/c3.min.js')}}"></script>
    <script src="{{ asset_admin('plugins/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset_admin('plugins/daterangepicker/daterangepicker.js')}}"></script>

    <script>
        var month = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var start = moment().subtract(30, 'days');
        var end = moment();

        var account_data_link = "{{ route_admin('report.account_data') }}";
        var post_data_link = "{{ route_admin('report.post_data') }}";

        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }


        function loadChart(data, data_link, time) {
            data.load({
                url: data_link + '?time=' + time + '&start=' + $('#start-date').val() + '&end=' + $('#end-date').val(),
                mimeType: 'json',
                duration: 1000
            });
        }

        function unloadDoneChart(data, data_link, time) {
            data.unload({
                done: function () {
                    loadChart(data, data_link, time);
                }
            });
        }

        $(function () {
            cb(start, end);

            // account chart
            var account = c3.generate({
                bindto: '#account',
                data: {
                    x: 'Time',
                    url: account_data_link,
                    type: 'bar',
                    mimeType: 'json',
                },
                color: {
                    pattern: ['#5b9bd5', '#ed7d31']
                },
                axis: {
                    x: {
                        type: 'timeseries',
                        tick: {
                            format: function (x) {
                                if ($('#account').closest('.box1').find('.date-filter li.active a').data('time') == 'month') {
                                    return month[x.getMonth()];
                                }
                                return x.getDate() + '-' + month[x.getMonth()];
                            }
                        }
                    },
                    y: {
                        tick: {
                            format: d3.format(',')
                        }
                    }
                },
                bar: {
                    width: {
                        ratio: 0.5
                    }
                },
                tooltip: {
                    format: {
                        value: d3.format(',')
                    }
                },
                legend: {
                    show: true,
                    position: 'bottom'
                },
            });

            let custom_datepicker = {
                autoApply: true,
                startDate: start,
                endDate: end,
                showWeekNumbers: true,
                format: 'DD/MM/YYYY',
                locale: {
                    fromLabel: 'Từ ngày',
                    toLabel: 'Đến ngày',
                    customRangeLabel: 'Tùy chỉnh ngày',
                    weekLabel: 'Tuần',
                    applyLabel: 'Đồng ý',
                    cancelLabel: 'Hủy',
                    format: 'DD/MM/YYYY',
                    "daysOfWeek": [
                        "CN",
                        "2",
                        "3",
                        "4",
                        "5",
                        "6",
                        "7"
                    ],
                    "monthNames": [
                        "Tháng 1",
                        "Tháng 2",
                        "Tháng 3",
                        "Tháng 4",
                        "Tháng 5",
                        "Tháng 6",
                        "Tháng 7",
                        "Tháng 8",
                        "Tháng 9",
                        "Tháng 10",
                        "Tháng 11",
                        "Tháng 12"
                    ],
                },
                ranges: {
                    'Hiện tại': [moment(), moment()],
                    '30 ngày gần nhất': [moment().subtract(30, 'days'), moment()],
                    'Trong tháng': [moment().startOf('month'), moment().endOf('month')],
                    'Tháng trước': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                    '3 Tháng trước': [moment().subtract(3, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }

            $('#reportrange').daterangepicker(custom_datepicker, cb).on('apply.daterangepicker', function (ev, picker) {
                $('#start-date').val(picker.startDate.format('YYYY-MM-DD'));
                $('#end-date').val(picker.endDate.format('YYYY-MM-DD'));
                account.unload({
                    done: function () {
                        var time = $('#account-filter li.active a').data('time');
                        loadChart(account, account_data_link, time)
                    }
                });
            });


            $('.date-filter li a').click(function () {
                $(this).closest('.date-filter').find('li').removeClass('active');
                $(this).parent().addClass('active');
                var time = $(this).data('time');
                var type = $(this).data('type').toString();
                unloadDoneChart(account, account_data_link, time);
            });
        })
    </script>

@endpush
