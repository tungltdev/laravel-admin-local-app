@extends('admin.layouts.master')

@section('content')

    <div class="row wrapper border-bottom white-bg page-heading mb-4">
        <div class="col-12">
            <h2>Trang thống kê Google Analytic</h2>
        </div>
    </div>

    <div class="row mt-4">
        <div class="col-12">
            <a href="{{route_admin('system.refresh.cache',\App\Constants\GoogleAnalyticConstant::CACHE_KEY)}}">
                <button class="btn btn-success dim" type="button">
                    Lấy dữ liệu mới nhất
                </button>
            </a>
        </div>
    </div>

    <div class="row mb-4">
        <div class="col-12 col-md-6 col-lg-3 col-xl-3">
            <div class="ibox-content card-thong-ke">
                <i class="fa fa-user float-right"></i>
                <h5 class="text-white text-uppercase">Người đang hoạt động</h5>
                <h2>{{@number_format(@$live_users)}}</h2>
            </div>
        </div>
    </div>
    <hr>

    <div class="row mb-4">
        <div class="col-md-6">
            <div class="box1" style="min-height: 400px;">
                <div class="float-left">
                    <h4>Truy cập và xem trang</h4>
                </div>
                <div class="float-right">
                    <ul class="date-filter" id="account-filter">
                        <li class="active">
                            <a href="javascript:void(0)" data-time="day"
                               data-type="account">{{ __('dashboard.day') }}</a></li>
                        <li><a href="javascript:void(0)" data-time="week"
                               data-type="account">{{ __('dashboard.week') }}</a></li>
                        <li><a href="javascript:void(0)" data-time="month"
                               data-type="account">{{ __('dashboard.month') }}</a></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <canvas id="pageViewsChart" style="height:250px"></canvas>
            </div>
        </div>

        <div class="col-md-6">
            <div class="box1" style="min-height: 400px;">
                <div class="float-left">
                    <h4>Thiết bị truy cập</h4>
                </div>
                <div class="float-right">
                    <ul class="date-filter" id="account-filter">
                        <li class="active">
                            <a href="javascript:void(0)" data-time="day"
                               data-type="account">{{ __('dashboard.day') }}</a></li>
                        <li><a href="javascript:void(0)" data-time="week"
                               data-type="account">{{ __('dashboard.week') }}</a></li>
                        <li><a href="javascript:void(0)" data-time="month"
                               data-type="account">{{ __('dashboard.month') }}</a></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
                <canvas id="device_pieChart" style="height:250px"></canvas>
            </div>
        </div>
    </div>
@stop

@push('styles')
    <style>
        .thong_ke_tong div {
            margin-top: 10px;
        }

        .card-thong-ke {
            background-color: #009444;
            color: white;
        }

        #gridTable .DTTTFooter {
            display: block;
        }

        .box1 {
            margin-bottom: 15px;
            padding: 10px 15px;
            background-color: #fff;
        }

        .box1.table {
            padding: 0;
        }

        .box1.table table {
            margin-bottom: 0;
        }

        .date-filter li {
            display: inline-block;
        }

        .date-filter li a {
            padding: 10px;
            background: #fbfcfc;
            border: 1px solid #d9d9d9;
            display: inline-block;
            color: #3e3d3e;
            font-weight: bold;
        }

        .date-filter li.active a, .date-filter li a:hover {
            background: #e4e4e4;
        }
    </style>
@endpush

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.1.1/Chart.min.js"></script>

    <script>
        $(function () {

            var pieChartCanvas = $("#device_pieChart").get(0).getContext("2d");
            var pieChart = new Chart(pieChartCanvas);
            var PieData = {!! $browserjson !!};
            var pieOptions = {
                //Boolean - Whether we should show a stroke on each segment
                segmentShowStroke: true,
                //String - The colour of each segment stroke
                segmentStrokeColor: "#fff",
                //Number - The width of each segment stroke
                segmentStrokeWidth: 2,
                //Number - The percentage of the chart that we cut out of the middle
                percentageInnerCutout: 50, // This is 0 for Pie charts
                //Number - Amount of animation steps
                animationSteps: 100,
                //String - Animation easing effect
                animationEasing: "easeOutBounce",
                //Boolean - Whether we animate the rotation of the Doughnut
                animateRotate: true,
                //Boolean - Whether we animate scaling the Doughnut from the centre
                animateScale: false,
                //Boolean - whether to make the chart responsive to window resizing
                responsive: true,
                // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio: true,
                //String - A legend template

            };

            pieChart.Doughnut(PieData, pieOptions);


            var barChartCanvas = $("#pageViewsChart").get(0).getContext("2d");
            var barChart = new Chart(barChartCanvas);
            var barChartData = {
                labels: {!! json_encode($dates->map(function($date) { return $date->format('d/m'); })) !!},

                datasets: [
                    {
                        label: "Xem trang",
                        fillColor: "rgba(210, 214, 222, 1)",
                        strokeColor: "rgba(210, 214, 222, 1)",
                        pointColor: "rgba(210, 214, 222, 1)",
                        pointStrokeColor: "#c1c7d1",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(220,220,220,1)",
                        data: {!! json_encode($pageViews) !!}
                    },
                    {
                        label: "Truy cập",
                        fillColor: "rgba(60,141,188,0.9)",
                        strokeColor: "rgba(60,141,188,0.8)",
                        pointColor: "#3b8bba",
                        pointStrokeColor: "rgba(60,141,188,1)",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(60,141,188,1)",
                        data: {!! json_encode($visitors) !!}
                    }
                ]
            };
            barChartData.datasets[1].fillColor = "#00a65a";
            barChartData.datasets[1].strokeColor = "#00a65a";
            barChartData.datasets[1].pointColor = "#00a65a";
            var barChartOptions = {
                scaleBeginAtZero: true,
                scaleShowGridLines: true,
                scaleGridLineColor: "rgba(0,0,0,.05)",
                scaleGridLineWidth: 1,
                scaleShowHorizontalLines: true,
                scaleShowVerticalLines: true,
                barShowStroke: true,
                barStrokeWidth: 2,
                barValueSpacing: 5,
                barDatasetSpacing: 1,

                responsive: true,
                maintainAspectRatio: true
            };

            barChartOptions.datasetFill = false;
            barChart.Bar(barChartData, barChartOptions);
        });
    </script>

@endpush
