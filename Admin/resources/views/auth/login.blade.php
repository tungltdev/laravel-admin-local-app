@extends('admin::auth.master')

@section('content')
    <h3>{{ __('Login') }} Admin</h3>
    <p>
        @include('admin::common._flash_message')
    </p>
    {!! Form::open(array('url' => route('admin.login'), 'method' => 'post','class'=>'m-t f_validate')) !!}
    <div class="form-group">
        <input type="text" class="form-control @error('username') is-invalid @enderror"
               name="username" value="{{ old('username') }}" placeholder="{{ __('admin.username') }}" required
               autocomplete="username" autofocus>
        @error('email')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
    <div class="form-group">
        <input type="password" class="form-control @error('password') is-invalid @enderror" required
               name="password" placeholder="{{ __('admin.password') }}">
        @error('password')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>

    {{--<div class="form-group text-left">--}}

    {{--<div class="i-checks">--}}
    {{--<label for="remember">--}}
    {{--<div class="icheckbox_square-green">--}}
    {{--<input name="remember" id="remember" type="checkbox" {{ old('remember') ? 'checked' : '' }}>--}}
    {{--</div>--}}
    {{--{{ __('Remember Me') }}--}}
    {{--</label>--}}
    {{--</div>--}}
    {{--</div>--}}
    <button type="submit" class="btn btn-primary block full-width m-b"> {{ __('Login') }} </button>
    {!! Form::close() !!}
    {{--<p class="m-t">--}}
    {{--<small>Admin Bootstrap 4 &copy; {{date('Y')}}</small>--}}
    {{--</p>--}}
@endsection

{{--@push('css')--}}
{{--<link href="{{asset('css/plugins/iCheck/custom.css')}}" rel="stylesheet">--}}
{{--@endpush--}}

{{--@push('js')--}}
{{--<script src="{{asset('js/plugins/iCheck/icheck.min.js')}}"></script>--}}
{{--<script>--}}
{{--$('.i-checks').iCheck({--}}
{{--checkboxClass: 'icheckbox_square-green',--}}
{{--radioClass: 'iradio_square-green',--}}
{{--});--}}
{{--</script>--}}
{{--@endpush--}}

