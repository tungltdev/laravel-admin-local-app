@extends('admin::layouts.index')

@section('body')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Danh sách nhóm quyền</h5>
                </div>
                <div class="ibox-content">
                    <div class="noi_dung_table">
                        <div class="gridTable"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

