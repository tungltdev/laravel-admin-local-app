<div class="row">
    <div class="col-12">
        <div class="ibox">
            <div class="ibox-title">
                <h5>Sửa nhóm quyền</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="form-group @error('name') has-error @enderror">
                                {!! Form::label('name', __("$type.name"), array('class' => ' required')) !!}
                                {!! Form::text('name', null, array('class' => 'form-control',
                                'placeholder'=> __("$type.name"),
                                'required' => true,
                                'minlength'=>$constant::VALIDATE['name_min_len'],
                                'maxlength'=>$constant::VALIDATE['name_max_len'])) !!}
                                <span class="help-block">{{ $errors->first('name', ':message') }}</span>
                            </div>
                        </div>

                        <div class="col-12 col-md-6">
                            <div class="form-group @error('guard_name') has-error @enderror">
                                {!! Form::label('guard_name', __("$type.guard_name"),array('class' => ' required')) !!}
                                {!! Form::select('guard_name', $constant::GUARD_NAME,null, array('class' => 'form-control')) !!}
                                <span class="help-block">{{ $errors->first('guard_name', ':message') }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="form-group @error('permission_ids') has-error @enderror">
                                {!! Form::label('permission_ids', __("$type.permissions"), array('class' => 'col-form-label required')) !!}
                                {!! Form::select('permission_ids[]', $permissions_pluck, @$doc->permissions, ['class' => 'form-control dual_select','multiple'=>true]) !!}
                                <span class="help-block">{{ $errors->first('permission_ids', ':message') }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="ibox-footer">
                @include('admin::common._button_submit')
            </div>
        </div>
    </div>
</div>
@push('css')
    <link href="{{asset_admin('plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{asset_admin('css/glyphicons.css')}}" rel="stylesheet">
    <link href="{{asset_admin('plugins/dualListbox/bootstrap-duallistbox.min.css')}}" rel="stylesheet">
@endpush

@push('js')
    <!-- iCheck -->
    <script src="{{asset_admin('plugins/iCheck/icheck.min.js')}}"></script>
    <script src="{{asset_admin('plugins/dualListbox/jquery.bootstrap-duallistbox.js')}}"></script>

    <script>
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        $('.dual_select').bootstrapDualListbox({
            selectorMinimalHeight: 160,
            filterPlaceHolder: 'Tìm kiếm',
            infoText: 'Hiện thị {0}',
            infoTextFiltered: '<span class="label label-warning">Kết quả lọc</span> {0} đến {1}',
            infoTextEmpty: 'Trống',
            filterTextClear: 'Xóa lọc',
            preserveSelectionOnMove: false,
            moveOnSelect: false,
        });
    </script>
@endpush
