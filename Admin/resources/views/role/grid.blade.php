<div class="table-responsive">
    <table class="data-table table table-striped table-bordered">
        <thead>
        <tr>
            <th class="table_stt sort" data-sort="id">ID</th>
            <th class="sort" data-sort="name">{{__("admin::$type.name")}}</th>
            <th>{{__("admin::$type.count_admin_use")}}</th>
            <th>{{__("admin::$type.count_permissions")}} / Tổng ({{count($permissions_pluck)}})</th>
            <th class="sort" data-sort="created_at">Ngày tạo</th>
            <th class="sort" data-sort="updated_at">Ngày sửa cuối</th>
            @include('admin::common.th_action_table')
        </tr>
        </thead>
        <tbody>
        @if(count($docs))
            @foreach($docs as $key => $doc)
                <tr>
                    <td class="table_id"> {{$doc->id}} </td>
                    <td>{{$doc->name}}</td>
                    <td title="{{@str_replace(array('[',']','"'),'', $doc->users()->pluck('username'))}}">{{$doc->users_count}}</td>
                    <td title="{{@str_replace(array('[',']','"'),'', $doc->permissions()->pluck('name'))}}">{{$doc->permissions_count}}</td>
                    <td>{{$doc->created_at->format($constant::FORMAT_DATE_TIME)}}</td>
                    <td>{{$doc->updated_at->format($constant::FORMAT_DATE_TIME)}}</td>
                    @include('admin::common.th_edit_delete_table')
                </tr>
            @endforeach
        @else
            <tr>
                <td class="no-data"></td>
            </tr>
        @endif
        </tbody>
    </table>
</div>

@include('admin::common.gridFooter')
