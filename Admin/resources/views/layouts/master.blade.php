<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" xmlns="">
<head>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @auth('admin')
        {{-- JWT Token --}}
        {{--<meta name="jwt-token" content="{{JWTAuth::fromUser(auth('admin')->user())}}">--}}
        <meta name="current-user-id" content="{{auth('admin')->id()}}">
    @endauth
    <title>@yield('title', @$title?:'title')</title>

    @if(!app()->environment('production') && config('debugbar.enabled'))
        @can('debugbar_view')
            @php
                $renderer = Debugbar::getJavascriptRenderer();
            @endphp
            {!! $renderer->renderHead() !!}
        @endcan
    @endif

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css"
          crossorigin="anonymous"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
          crossorigin="anonymous"/>

    <!-- Toastr style -->
    <link href="{{asset_admin('plugins/toastr/toastr.min.css')}}" rel="stylesheet">

    <link href="{{asset_admin('css/animate.css')}}" rel="stylesheet">
    <link href="{{asset_admin('css/style.css')}}" rel="stylesheet">
    <link href="{{asset_admin('css/custom.css')}}" rel="stylesheet">

    <!-- stack styles -->
@yield('css')
@stack('css')
<!-- ./stack styles -->

</head>

<body class="fixed-sidebar">

<div id="wrapper">
    @include('admin::layouts._menu')

    <div id="page-wrapper" class="gray-bg dashbard-1">

        @include('admin::layouts._header')

        {{--        <div class="row  border-bottom white-bg dashboard-header">--}}
        {{--            @yield('dashboard-header')--}}
        {{--        </div>--}}

        <div class="wrapper wrapper-content animated fadeInRight">
            @include('admin::common._flash_message')

            @yield('content')
        </div>

        @include('admin::layouts._footer')

    </div>

    {{--@include('admin.layouts._chat')--}}

    {{--@include('admin.layouts._right_sidebar')--}}

</div>
</body>
</html>
@if(!app()->environment('production') && config('debugbar.enabled'))
    @can('debugbar_view')
        {!! $renderer->render() !!}
    @endcan
@endif
<!-- script -->

<!-- stack top_scripts -->
@stack('top_scripts')
<!-- ./stack top_scripts -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js" crossorigin="anonymous"></script>

<!-- Mainly scripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" crossorigin="anonymous"></script>
<script src="{{asset_admin('js/popper.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"
        crossorigin="anonymous"></script>
<script src="{{asset_admin('plugins/metisMenu/jquery.metisMenu.js')}}"></script>
<script src="{{asset_admin('plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset_admin('plugins/moment/moment.js') }}"></script>

<!-- Toastr -->
<script src="{{asset_admin('plugins/toastr/toastr.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
{{--@auth('admin')--}}
{{--<script type="text/javascript" src="{{ mix('js/admin/chat.js') }}"></script>--}}
{{--<script type="text/javascript" src="{{ mix('js/admin/notifications.js') }}"></script>--}}
{{--@endauth--}}
<script src="{{asset_admin('js/inspinia.js')}}"></script>

<script>
    $(function () {
        setInterval(function () {
            $.ajax({
                url: '{{ route_admin('profile.refresh') }}',
                type: 'get',
                success: function (data) {
                    console.log(data)
                }
            });
        }, 120000)
    })
</script>


@include('admin::layouts._script')
<!-- stack scripts -->
@yield('js')
@stack('js')
<!-- ./stack scripts -->

<!-- ./script -->
