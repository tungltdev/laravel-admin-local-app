@extends('admin::layouts.app')

@section('content')
    @yield('head',view('admin::common._button_add_table'))
    {!! Form::open(array('url' => "$url_current/grid", 'method' => 'post', 'class'=>'gridForm autoload')) !!}
    <input type="hidden" name="sort" class="sortTable" value="{{getGridInputOld('sort')}}"/>
    @yield('body')
    {!! Form::close() !!}
@endsection
