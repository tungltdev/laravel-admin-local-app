<div class="footer">
    <div>
        <strong>{{config('admin.name')}}</strong> | {{\Carbon\Carbon::now()}}
    </div>
</div>

@if(config('faker_user.enabled'))
    @includeIf('faker_user::user-selector')
@endif
