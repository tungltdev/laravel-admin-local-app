<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <a href="/" style="margin: 0 auto;" target="_blank">
                <img src="{{asset_admin('img/logo/logo1.png')}}" class="img-fluid logo-dashboard-admin">
            </a>
            <li class="nav-header info-user-menu-left">
                <div class="media">
                    <a class="pull-left " href="{{route_admin('profile')}}">
                        <img src="{{getImageStorage(@$admin->image)}}" width="50" height="50"
                             style="border-radius: 5px">
                    </a>
                    <div class="content-profile" style="margin-left: 5px">
                        <h4 class="text-white">{{$admin->name}} </h4>
                        <h5 style="color: #C1CBCC; word-break: break-all;margin-top: 5px">{{!empty(@count($admin->roles))?$admin->roles->pluck("name"):''}}</h5>
                    </div>
                </div>
            </li>
            <li {!! activeMenuAdmin('index')? 'class="active"' : '' !!}>
                <a href="{{route_admin('index')}}">
                    <i class="text-info fa fa-home"></i>
                    <span class="nav-label ng-binding">Dashboard</span>
                </a>
            </li>

            @canany(['job_view'])
                <li {!! (activeMenuAdmin('job*'))? 'class="active"' : '' !!}>
                    <a href="javascript:void(0)">
                        <i class="text-warning fa fa-compass"></i>
                        <span class="nav-label ng-binding">Quản trị việc làm</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse">
                        @can('job_view')
                            <li {!! activeMenuAdmin('job*')? 'class="active"' : '' !!}>
                                <a href="{{route_admin('job.index')}}">
                                    <span class="nav-label">Danh sách việc làm</span>
                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcanany

            @canany(['candidate_view'])
                <li {!! activeMenuAdmin('candidate*')? 'class="active"' : '' !!}>
                    <a href="javascript:void(0)">
                        <i class="text-info fa fa-compass"></i>
                        <span class="nav-label ng-binding">Quản trị hồ sơ</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse">
                        @can('candidate_view')
                            <li {!! activeMenuAdmin('candidate.index')? 'class="active"' : '' !!}>
                                <a href="{{route_admin('candidate.index')}}">
                                    <span class="nav-label ng-binding">Danh sách hồ sơ</span>
                                </a>
                            </li>
                        @endcan
                        @can('candidate_view_list_status_call')
                            <li {!! activeMenuAdmin('candidate.index_status_call')? 'class="active"' : '' !!}>
                                <a href="{{route_admin('candidate.index_status_call')}}">
                                    <span class="nav-label ng-binding">Danh sách hồ sơ đã gọi</span>
                                </a>
                            </li>
                        @endcan
                        @can('candidate_view_list_support_complete')
                            <li {!! activeMenuAdmin('candidate.index_sale_support_complete')? 'class="active"' : '' !!}>
                                <a href="{{route_admin('candidate.index_sale_support_complete')}}">
                                    <span class="nav-label ng-binding">Danh sách hồ sơ hỗ trợ hoàn thành</span>
                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcanany

            @canany(['admin_view','user_view','role_view','permission_view'])
                <li {!! ((activeMenuAdmin('user*'))||activeMenuAdmin('admin*')||activeMenuAdmin('role*')||activeMenuAdmin('permission*'))? 'class="active"' : '' !!}>
                    <a href="javascript:void(0)">
                        <i class="text-danger fa fa-bar-chart-o"></i>
                        <span class="nav-label">Quản trị người dùng</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse">
                        @can('admin_view')
                            <li {!! activeMenuAdmin('admin*')? 'class="active"' : '' !!}>
                                <a href="{{route_admin('admin.index')}}">
                                    Danh sách Quản trị
                                </a>
                            </li>
                        @endcan
                        @can('user_view')
                            <li {!! (activeMenuAdmin('user*') && !activeMenuAdmin('user_company*'))? 'class="active"' : '' !!}>
                                <a href="{{route_admin('user.index')}}">
                                    Danh sách Người dùng
                                </a>
                            </li>
                        @endcan
                        @can('role_view')
                            <li {!! activeMenuAdmin('role*')? 'class="active"' : '' !!}>
                                <a href="{{route_admin('role.index')}}">
                                    Danh sách nhóm quyền
                                </a>
                            </li>
                        @endcan
                        @can('permission_view')
                            <li {!! activeMenuAdmin('permission*')? 'class="active"' : '' !!}>
                                <a href="{{route_admin('permission.index')}}">
                                    Danh sách Quyền hạn
                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcanany

            @canany(['company_view'])
                <li {!! (activeMenuAdmin('company*'))? 'class="active"' : '' !!}>
                    <a href="javascript:void(0)">
                        <i class="text-warning fa fa-bar-chart-o"></i>
                        <span class="nav-label">Quản trị HR</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse">
                        @can('company_view')
                            <li {!! activeMenuAdmin('company*')? 'class="active"' : '' !!}>
                                <a href="{{route_admin('company.index')}}">
                                    Danh sách công ty
                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcanany

            @canany(['service_view','service_request_view','service_box_view'])
                <li {!! (activeMenuAdmin('service*') || activeMenuAdmin('service_box*') || activeMenuAdmin('service_request*'))? 'class="active"' : '' !!}>
                    <a href="javascript:void(0)">
                        <i class="text-warning fa fa-bar-chart-o"></i>
                        <span class="nav-label">Quản trị dịch vụ</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse">
                        @can('service_request_view')
                            <li {!! activeMenuAdmin('service_request*')? 'class="active"' : '' !!}>
                                <a href="{{route_admin('service_request.index')}}">
                                    Danh sách yêu cầu dịch vụ
                                </a>
                            </li>
                        @endcan
                        @can('service_view')
                            <li {!! (!activeMenuAdmin('service_request*')&&!activeMenuAdmin('service_box*')&&!activeMenuAdmin('service_log*')&&!activeMenuAdmin('service-product*')&&activeMenuAdmin('service*'))? 'class="active"' : '' !!}>
                                <a href="{{route_admin('service.index')}}">
                                    Danh sách set dịch vụ
                                </a>
                            </li>
                        @endcan
                        @can('service_box_view')
                            <li {!! (activeMenuAdmin('service_box*')&&!activeMenuAdmin('service.index'))? 'class="active"' : '' !!}>
                                <a href="{{route_admin('service_box.index')}}">
                                    Danh sách việc làm dịch vụ
                                </a>
                            </li>
                        @endcan
                        @can('service_log_view')
                            <li {!! (activeMenuAdmin('service_log*')&&!activeMenuAdmin('service.index'))? 'class="active"' : '' !!}>
                                <a href="{{route_admin('service_log.index')}}">
                                    Danh sách log các lần set dịch vụ
                                </a>
                            </li>
                        @endcan
                        @can('service_product_view')
                            <li {!! activeMenuAdmin('service-product*')? 'class="active"' : '' !!}>
                                <a href="{{route_admin('service-product.index')}}">
                                    Danh sách báo giá dịch vụ
                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcanany

            @canany(['statistical_view','thongke_thongKeChoSeo','thongke_thongKeChoMarketing'])
                <li {!! (activeMenuAdmin('statistical*') || activeMenuAdmin('thongke.thongKeChoSeo*')|| activeMenuAdmin('thongke.thongKeChoMarketing*'))? 'class="active"' : '' !!}>
                    <a href="javascript:void(0)">
                        <i class="text-success fa fa-diamond"></i>
                        <span class="nav-label">Quản trị thống kê</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse">
                        <li {!! activeMenuAdmin('statistical.candidate')? 'class="active"' : '' !!}>
                            <a href="{{route_admin('statistical.candidate')}}">
                                <span class="nav-label ng-binding">Thống kê ứng viên</span>
                            </a>
                        </li>
                        <li {!! activeMenuAdmin('statistical.candidateCategory')? 'class="active"' : '' !!}>
                            <a href="{{route_admin('statistical.candidateCategory')}}">
                                <span class="nav-label ng-binding">Thống kê ứng viên theo danh mục</span>
                            </a>
                        </li>
                        <li {!! activeMenuAdmin('statistical.candidateJob')? 'class="active"' : '' !!}>
                            <a href="{{route_admin('statistical.candidateJob')}}">
                                <span class="nav-label ng-binding">Thống kê việc làm theo danh mục</span>
                            </a>
                        </li>
                        <li {!! activeMenuAdmin('statistical.statisticalCategory')? 'class="active"' : '' !!}>
                            <a href="{{route_admin('statistical.statisticalCategory')}}">
                                <span class="nav-label ng-binding">Thống kê theo danh mục</span>
                            </a>
                        </li>
                        <li {!! activeMenuAdmin('statistical.job')? 'class="active"' : '' !!}>
                            <a href="{{route_admin('statistical.job')}}">
                                <span class="nav-label ng-binding">Thống kê NTD - Ứng tuyển</span>
                            </a>
                        </li>
                        <li {!! activeMenuAdmin('statistical.source_create_cv')? 'class="active"' : '' !!}>
                            <a href="{{route_admin('statistical.source_create_cv')}}">
                                <span class="nav-label ng-binding">Thống kê nguồn tạo cv (Ads)</span>
                            </a>
                        </li>

                        @can('thongke_thongKeChoMarketing')
                            <li {!! activeMenuAdmin('thongke.thongKeChoMarketing*')? 'class="active"' : '' !!}>
                                <a href="{{route_admin('thongke.thongKeChoMarketing')}}">
                                    <span class="nav-label ng-binding">Thống kê cho Marketing</span>
                                </a>
                            </li>
                        @endcan

                        @can('thongke_thongKeChoSeo')
                            <li {!! activeMenuAdmin('thongke.thongKeChoSeo*')? 'class="active"' : '' !!}>
                                <a href="{{route_admin('thongke.thongKeChoSeo')}}">
                                    <span class="nav-label ng-binding">Thống kê cho SEO</span>
                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcanany

            <li {!! activeMenuAdmin('profile')? 'class="active"' : '' !!}>
                <a href="{{route_admin('profile')}}">
                    <i class="text-success fa fa-user"></i>
                    <span class="nav-label ng-binding">Trang cá nhân</span>
                </a>
            </li>


            @canany(['category_view','tag_view','city_view','system_setting','system_view','log_viewer_view'])
                <li {!! (activeMenuAdmin('system*') || activeMenuAdmin('category*') || activeMenuAdmin('tag*') || activeMenuAdmin('city*') || activeMenuAdmin('log.index'))? 'class="active"' : '' !!}>
                    <a href="javascript:void(0)">
                        <i class="text-success fa fa-diamond"></i>
                        <span class="nav-label">Hệ thống</span>
                        <span class="fa arrow"></span>
                    </a>
                    <ul class="nav nav-second-level collapse">
                        @can('system_view')
                            <li {!! activeMenuAdmin('system.info')? 'class="active"' : '' !!}>
                                <a href="{{route_admin('system.info')}}">
                                    <i class="fa fa-eye"></i>
                                    <span class="nav-label ng-binding">Theo dõi hệ thống</span>
                                </a>
                            </li>
                        @endcan
                        @can('system_setting')
                            <li {!! activeMenuAdmin('system.setting')? 'class="active"' : '' !!}>
                                <a href="{{route_admin('system.setting')}}">
                                    <i class="fa fa-cog"></i>
                                    <span class="nav-label ng-binding">Cài đặt hệ thống</span>
                                </a>
                            </li>
                        @endcan
                        @can('log_viewer_view')
                            <li {!! activeMenuAdmin('log.index')? 'class="active"' : '' !!}>
                                <a href="{{route_admin('log.index')}}">
                                    <i class="fa fa-eye"></i>
                                    <span class="nav-label ng-binding">Theo dõi log</span>
                                </a>
                            </li>
                        @endcan
                    </ul>
                </li>
            @endcanany
        </ul>
    </div>
</nav>
