@extends('admin::layouts.master')

@push('css')
    <link rel="stylesheet" href="{{ asset_admin('plugins/bootstrapValidator/bootstrapValidator.min.css') }}"/>
@endpush

@push('js')
    <script src="{{ asset_admin('plugins/bootstrapValidator/bootstrapValidator.min.js') }}"></script>
    <script src="{{ asset_admin('plugins/bootstrapValidator/lang/vi_VN.js') }}"></script>
@endpush

@push('js')
    <script>
        $(function () {
            // validate form
            $('form.f_validate').bootstrapValidator({
                excluded: [':disabled'],
                // icon: {
                //     valid: 'glyphicon glyphicon-ok',
                //     invalid: 'glyphicon glyphicon-remove',
                //     validating: 'glyphicon glyphicon-refresh'
                // }
            }).on('err.field.fv', function (e, data) {
                var $invalidFields = data.fv.getInvalidFields().eq(0);
                var $tabPane = $invalidFields.parents('.tab-pane');
                // invalidTabId = $tabPane.attr('id');
                if (!$tabPane.hasClass('active')) {
                    $invalidFields.focus();
                }
            }).on('keyup keypress', function (e) {
                // var code = e.keyCode || e.which;
                // if (code == 13 && !$(e.target).is("textarea") && $(e.target).closest('form').attr('id') != 'search-form' && $(e.target).closest('form').attr('id') != 'filter-form') {
                //     e.preventDefault();
                //     return false;
                // }
            });
        })
    </script>
@endpush

