<script>
    $.fn.button = function (action) {
        if (action === 'loading' && this.data('loading-text')) {
            this.data('original-text', this.html()).html(this.data('loading-text')).prop('disabled', true);
        }
        if (action === 'reset' && this.data('original-text')) {
            this.html(this.data('original-text')).prop('disabled', false);
        }
    };

    $('input').dblclick(function () {
        $(this).select();
    });

    var timeoutSearch;
    $(".gridForm.autoload .change_grid:not('.date')").on('keyup', function () {
        timeoutSearch && clearTimeout(timeoutSearch);
        timeoutSearch = setTimeout(() => {
            $('.gridForm').submit();
        }, 500)
    });

    $('.gridForm.autoload select.change_grid').on('change', function () {
        $('.gridForm').submit();
    });
    //
    // $('.gridForm.autoload input.check[type=checkbox]').on('ifChanged', function() {
    //     $('.gridForm').submit();
    // });
    //
    // $('.gridForm.autoload input.change_grid.date').on('dp.hide', function() {
    //     $('.gridForm').submit();
    // });
    $body = $('body');
    $body.delegate(".gridForm.autoload .row button[type='submit']", 'click', function () {
        $(".reset_grid").remove();
        $('.gridForm').submit();
        let $btn = $(this);
        $btn.button('loading');
        setTimeout(function () {
            $btn.button('reset');
        }, 800);
    });

    $body.delegate(".gridForm.autoload .row button[type='reset']", 'click', function () {
        $(".gridForm input[type!='hidden'],.gridForm select:not('.select_limit')").val('');
        $('select.chosen-select.change_grid').val('').trigger('chosen:updated');
        $('<input>').attr({type: 'hidden', class: 'reset_grid', name: 'reset_grid', value: 1}).appendTo('.gridForm');
        $('.gridForm').submit();
        let $btn = $(this);
        $btn.button('loading');
        setTimeout(function () {
            $btn.button('reset');
        }, 800);
    });

    $body.delegate(".sort", 'click', function () {
        var sort = $(this).data('sort');
        $sortTable = $('.sortTable');
        if($sortTable.val()){
            if ($sortTable.val() == sort) {
                if (sort[0] === "-") {
                    sort = sort.substr(1);
                }
                sort = "-" + sort;
            }
        }else{
            sort = "-" + sort;
        }
        $sortTable.val(sort);
        $('.gridForm').submit();
    });

    $(".gridForm.autoload").submit(function (e, page) {
        if (!page)
            page = 1;

        $('input[name=page]').val(page);
        let url_current = "{{@$url_current}}?" + $('input[name!=_token],select', this).serialize();
        window.history.pushState("", "", url_current);
        let $gridTable = $(this).find('.gridTable');
        var data_from = $(this).serialize();
        $gridTable.empty();
        $gridTable.append('<div class="loading"></div>');
        $.post($(this).attr('action'), data_from).done(function (data) {
            $gridTable.html(data);
            $nodata = $gridTable.find('.no-data');
            if ($nodata)
                $nodata.attr('colspan', $('table tr th').length).html(`Không có bản ghi phù hợp`);
        })
            .fail(function () {
                alert("Có lỗi xảy ra, vui lòng kiểm tra lại");
            });
        return false;

    }).trigger("submit", [{{getGridInputOld('page')?(int)getGridInputOld('page'):1}}]);

    @if(isset($title))
    $('body').on('click', '.delete_item', function () {
        var str = $(this).data('id');
        if ($(this).data('name')) {
            str = $(this).data('name');
        }

        Swal.fire({
            html: 'Delete {{ strtolower($title) }} "' + str + '" ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Xác nhận'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: '{{ url($url_current) }}/' + $(this).data('id'),
                    type: 'post',
                    data: {
                        '_method': 'delete',
                        '_token': '{{ csrf_token() }}',
                    },
                    success: function (data) {
                        if (data.ok) {
                            toastr.options = {
                                closeButton: true,
                                progressBar: true,
                                showMethod: 'slideDown',
                                timeOut: 3000
                            };
                            toastr.success(data.msg || 'Xóa thành công bản ghi', 'Thông báo');

                            setTimeout(function () {
                                location.reload();
                            }, 3000)
                        } else {
                            toastr.options = {
                                closeButton: true,
                                progressBar: true,
                                showMethod: 'slideDown',
                                timeOut: 3000
                            };
                            toastr.error(data.msg || "{{ __('message.cant_delete_item') }}", 'Thông báo xóa');
                        }
                    }
                })
            }
        })
    });

    @endif

    function showErrorAjax(request, title = ' ') {
        if (request.responseJSON.errors) {
            let loi = [];
            Object.keys(request.responseJSON.errors).forEach(key => {
                loi.push(request.responseJSON.errors[key][0] + "\n");
            });
            Swal.fire({
                icon: 'error',
                title: title,
                text: loi,
            })
        }
    }

    function imagePreview(input) {
        let pr = $(input).closest('div.img_upload');
        let img_pr = $(pr).find('img.img_upload_file');
        if (input.files && input.files[0]) {
            let reader = new FileReader();
            reader.onload = function (e) {
                $(img_pr).attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
