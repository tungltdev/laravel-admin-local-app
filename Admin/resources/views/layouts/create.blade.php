@extends('admin::layouts.app')

@section('content')
    @if (isset($doc))
        <!-- form sửa -->
        {!! Form::model($doc, array('url' => $link_edit, 'method' => 'put', 'files'=> true,'class'=>'f_validate')) !!}
    @else
        <!-- form thêm mới -->
        {!! Form::open(array('url' => $link_add, 'method' => 'post', 'files'=> true,'class'=>'f_validate')) !!}
    @endif
    @include("admin::$type/form")
    {!! Form::close() !!}
    <!-- end form -->
@endsection
