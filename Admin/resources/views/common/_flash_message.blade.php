@includeIf('flash::message')

@if($errors->any())
    <div class="alert-danger text-danger" style="margin-bottom: 10px">
        @foreach ($errors->all() as $error)
            {{$error}} <br>
        @endforeach
    </div>
@endif
