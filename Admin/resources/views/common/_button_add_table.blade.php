@can($type.'_create')
    <a href="{{route_admin("$type.create")}}">
        <button class="btn btn-primary" type="button">
            <i class="fa fa-plus-circle"></i> Thêm mới
        </button>
    </a>
@endcan
