@canany([$type."_edit",$type."_destroy"])
    <td class="table_action text-center">
        @can($type."_edit")
            <a href="{{ route_admin("$type.edit",@$doc->id) }}" title="{{__('table.edit')}}">
                <i class="fa fa-fw fa-pencil text-warning"></i>
                {{--{{__('table.edit')}}--}}
            </a>
        @endcan

        @can($type."_destroy")
            <a href="javascript:void(0)" data-id="{{@$doc->id}}"
               class="delete_item" title="{{__('table.delete')}}"><i
                        class="fa fa-fw fa-times text-danger"></i>
                {{--{{__('table.delete')}}--}}
            </a>
        @endcan
    </td>
@endcanany
