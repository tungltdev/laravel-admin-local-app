@php
    if(empty($grid['limit'])){
        @$grid['limit'] = 15;
    }
    if(empty($grid['page'])){
        @$grid['page'] = 1;
    }
@endphp

<div class="DTTTFooter" style="padding: 10px 15px;">
    <div>
        <input type="hidden" name="page"
               value="{{ (!empty(getGridInputOld('limit')) && getGridInputOld('page') > 0) ? getGridInputOld('page') : 1 }}"/>
        Hiện
        <select name="limit" class="input-xs select_limit" onchange="$('.gridForm.autoload').trigger('submit');"
                style="padding: 0;">
            <option {{ (getGridInputOld('limit') == 15) ? 'selected' : '' }} value="15">15</option>
            <option {{ (getGridInputOld('limit') == 30) ? 'selected' : '' }} value="30">30</option>
            <option {{ (getGridInputOld('limit') == 50) ? 'selected' : '' }} value="50">50</option>
            <option {{ (getGridInputOld('limit') == 100) ? 'selected' : '' }} value="100">100</option>
            <option {{ (getGridInputOld('limit') == 150) ? 'selected' : '' }} value="150">150</option>
            <option {{ (getGridInputOld('limit') == 200) ? 'selected' : '' }} value="200">200</option>
        </select> Bản ghi / 1 trang
    </div>
    <div style="clear: both"></div>
    <div>
        <div style="float: left;margin-top: 10px;">Đang xem
            từ {{(1 + getGridInputOld('limit') * (getGridInputOld('page') - 1))}} đến
            {{(@count(@$docs) + getGridInputOld('limit') * (getGridInputOld('page') - 1))}} của tổng số <b
                    style="color: red">{{@$docs->total()}}</b> bản ghi.
        </div>
        <div class="text-right" style="float: right;margin-top: 0">
            {!! getPagingAdmin(@$docs->total(), getGridInputOld('limit')?:15) !!}
        </div>
    </div>
    <div style="clear: both"></div>
</div>

<style>
    .pagination > li.active a {
        background-color: #dfe0e0 !important;
    }

    ul.pagination {
        margin: 0 !important;
    }
</style>
