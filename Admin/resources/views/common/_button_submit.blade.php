<div class="form-group text-right my-1">
    <div class="controls">
        <a href="{{ route_admin("$type.index") }}" class="btn btn-danger">
            <i class="fa fa-times"></i> {{__('form.back')}}
        </a>
        <button type="submit" class="btn btn-success">
            <i class="fa fa-check-square-o"></i> {{__('form.submit')}}
        </button>
    </div>
</div>
