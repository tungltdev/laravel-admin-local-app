<button type="reset" class="btn btn-default"
        data-loading-text="{{@$l_text1?:'Đang tìm...'}}">{{@$text1?:'Tìm lại'}}</button>
<button type="submit" class="btn btn-primary"
        data-loading-text="{{@$l_text2?:'Đang tìm...'}}">{{@$text2?:'Tìm kiếm'}}</button>
