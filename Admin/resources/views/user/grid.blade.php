<div class="table-responsive">
    <table id="data-table" class="table table-striped table-bordered">
        <thead>
        <tr>
            <th class="table_stt sort" data-sort="id">ID</th>
            <th>Tên họ</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Loại TK</th>
            <th>Đăng nhập lần cuối</th>
            <th>Xác minh</th>
            @canany([$type."_edit",$type."_destroy",$type."_verify_account"])
                <th class="text-center">Action</th>
            @endcanany
        </tr>
        </thead>
        <tbody>
        @if(count($docs))
            @foreach($docs as $key => $doc)
                <tr>
                    <td class="table_id"> {{$doc->id}} </td>
                    <td>{{$doc->name}}</td>
                    <td>{{$doc->phone}}</td>
                    <td>{{$doc->email}}</td>
                    <td>{{$doc->user_type_id==1?'Nhà tuyển dụng':'Ứng viên' }}</td>
                    <td>{{$doc->last_login_at?$doc->last_login_at->format('d/m/Y H:i:s'):'' }}</td>
                    <td>
                        @if(@$doc->is_verify_email == 1)
                            <span class="badge badge-success position-relative"
                                  data-toggle="tooltip" title="Tài khoản đã xác minh email"> <i class="fa fa-envelope"
                                                                                                aria-hidden="true"></i>
                                </span>
                        @endif

                        @if(@$doc->is_verify_phone == 1)
                            <span class="badge badge-info position-relative"
                                  data-toggle="tooltip" title="Tài khoản đã xác minh phone"> <i
                                        class="fa fa-phone-square" aria-hidden="true"></i>
                             </span>
                        @endif
                    </td>
                    @canany([$type."_edit",$type."_destroy",$type."_verify_account"])
                        <td class="table_action">
                            @can($type."_verify_account")
                                <a href="javascript:void(0)" data-id="{{@$doc->id}}" class="btn_verify_account"
                                   title="Xác thực tài khoản">
                                    <i class="fa fa-fw fa-check text-info"></i>
                                </a>
                            @endcan
                            @can($type."_edit")
                                <a href="{{ route_admin("$type.edit",@$doc->id) }}" title="{{__('table.edit')}}">
                                    <i class="fa fa-fw fa-pencil text-warning"></i>
                                    {{--{{__('table.edit')}}--}}
                                </a>
                            @endcan
                            @if($doc->user_type_id == 2)
                                @can($type."_destroy")
                                    <a href="javascript:void(0)" data-id="{{@$doc->id}}"
                                       class="delete_item" title="{{__('table.delete')}}"><i
                                                class="fa fa-fw fa-times text-danger"></i>
                                        {{--{{__('table.delete')}}--}}
                                    </a>
                                @endcan
                            @endif
                        </td>
                    @endcanany
                </tr>
            @endforeach
        @else
            <tr>
                <td class="no-data"></td>
            </tr>
        @endif
        </tbody>
    </table>
</div>

@include('admin::common.gridFooter')
