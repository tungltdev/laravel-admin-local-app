@extends('admin::layouts.index')

@section('body')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Danh sách người dùng</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="mb-1 col-4 col-md-3 col-xl-2">
                            <input value="{{getGridInputOld('id')}}" type="number" name="id"
                                   class="form-control change_grid"
                                   placeholder="ID">
                        </div>
                        <div class="mb-1 col-4 col-md-3 col-xl-3">
                            <div class="input-group">
                                <input name="search" placeholder="Tên, Email, Số điện thoại" type="text"
                                       class="form-control change_grid" value="{{getGridInputOld('search')}}">
                            </div>
                        </div>
                        <div class="mb-1 col-4 col-md-3 col-xl-3">
                            {!! Form::select('user_type_id', [''=>'Tất cả','1'=>'Nhà tuyển dụng','2'=>'Ứng viên'], getGridInputOld('user_type_id'), ['class' => 'chosen-select change_grid']) !!}
                        </div>
                        <div class="mb-1 col-4 col-md-3 col-xl-2">
                            {!! Form::text('start', getGridInputOld('start'), array('class' => 'form-control date','placeholder'=>'Từ ngày')) !!}
                        </div>
                        <div class="mb-1 col-4 col-md-3 col-xl-2">
                            {!! Form::text('end', getGridInputOld('end'), array('class' => 'form-control date','placeholder'=>'Đến ngày')) !!}
                        </div>
                        <div class="mb-1 col-6">
                            @include('admin::common._button_search_table')
                        </div>
                    </div>

                    <div class="noi_dung_table">
                        <div class="gridTable"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link href="{{ asset_admin('plugins/chosen/bootstrap-chosen.css')}}" rel="stylesheet">
    <link href="{{ asset_admin('plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css')}}"
          rel="stylesheet"/>
@endpush

@push('js')
    <script src="{{ asset_admin('plugins/chosen/chosen.jquery.js')}}"></script>
    <script src="{{ asset_admin('plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>

    <script>
        $(function () {
            $('.chosen-select').chosen({
                width: "100%",
                allow_single_deselect: true
            });


            $('.date').datetimepicker({
                format: 'Y-MM-DD',
                useCurrent: true,
                icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                }
            });
        });

        $('body').delegate('.btn_verify_account', 'click', function () {
            Swal.fire({
                html: 'Bạn có chắc chắn muốn xác thực tài khoản này không?',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Xác nhận'
            }).then((result) => {
                if (result.value) {
                    var user_id = $(this).data('id');
                    $.ajax({
                        headers: {'X-CSRF-TOKEN': '{{ csrf_token() }}'},
                        type: "POST",
                        url: '{{ url('ajax/admin/user/very_account') }}/' + user_id,
                        success: function (data) {
                            if (parseInt(data.status) == 1) {
                                Swal.fire('Xác thực tài khoản thành công!');
                                $('#gridForm').trigger("submit", [parseInt(data.page)]);
                            } else {
                                alert('Có lỗi xảy ra vui lòng thử lại.')
                            }
                        }
                        , error: function () {
                            alert('Có lỗi xảy ra vui lòng thử lại.')
                        }
                    });
                }
            })
        });

    </script>
@endpush
