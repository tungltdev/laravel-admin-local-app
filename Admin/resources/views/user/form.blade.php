<div class="row">
    <div class="col-12">
        <div class="ibox">
            <div class="ibox-title">
                <h5>Thêm mới người dùng</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="ibox-content">
                <div class="tabs-container">
                    <ul class="nav nav-tabs" role="tablist">
                        <li>
                            <a class="nav-link active show" data-toggle="tab" href="#tab-1">
                                Thông tin đăng nhập
                            </a>
                        </li>
                        <li>
                            <a class="nav-link" data-toggle="tab" href="#tab-2">
                                Thông tin cá nhân
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" id="tab-1" class="tab-pane active show">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group @error('username') has-error @enderror">
                                            {!! Form::label('username', __("admin::$type.username"), array('class' => ' required')) !!}
                                            {!! Form::text('username', null, array('class' => 'form-control',
                                            'placeholder'=> __("admin::$type.username"),'required' => true,
                                            'minlength'=>$constant::VALIDATE['username_min_len'],
                                            'maxlength'=>$constant::VALIDATE['username_max_len'])) !!}
                                            <span class="help-block">{{ $errors->first('username', ':message') }}</span>
                                        </div>
                                    </div>
                                </div>
                                @if (isset($doc))
                                    <div class="row">
                                        <div class="col-xs-12 col-md-6">
                                            <div class="form-group">
                                                <div class="i-checks">
                                                    <label for="remember">
                                                        <div class="icheckbox_square-green">
                                                            <input name="show_pass" id="show_pass" type="checkbox">
                                                        </div>
                                                        {{ __("admin::$type.change_password") }}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-6">
                                            <div class="form-group">
                                                <div class="i-checks">
                                                    <label for="remember">
                                                        <div class="icheckbox_square-green">
                                                            {!! Form::checkbox('blocked', null,@$doc->blocked) !!}
                                                        </div>
                                                        {{ __("admin::$type.blocked") }}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group @error('password') has-error @enderror password_field">
                                            {!! Form::label('password', __("admin::$type.password"),array('class'=>empty($doc)?"required":'')) !!}
                                            {!! Form::password('password', array('class' => 'form-control',empty($doc)?"required":'',
                                            'placeholder'=> __("admin::$type.password"),
                                            'data-bv-identical'=>true,
                                            'data-bv-identical-field'=>'confirmPassword',
                                            'minlength'=>$constant::VALIDATE['password_min_len'],
                                            'maxlength'=>$constant::VALIDATE['password_max_len']
                                            )) !!}
                                            <span class="help-block">{{ $errors->first('password', ':message') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <div
                                                class="form-group @error('password_confirmation') has-error @enderror password_field">
                                            {!! Form::label('password_confirmation', __("admin::$type.password_confirmation"),array('class'=>empty($doc)?"required":'')) !!}
                                            {!! Form::password('password_confirmation', array('class' => 'form-control',empty($doc)?"required":'',
                                            'placeholder'=> __("admin::$type.password_confirmation"),
                                            'data-bv-identical'=>true,
                                            'data-bv-identical-field'=>"password"
                                            )) !!}
                                            <span
                                                    class="help-block">{{ $errors->first('password_confirmation', ':message') }}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="img_upload">
                                            <img class="img_upload_file"
                                                 src="{{getImageStorage(@$doc->photo)}}"/>
                                            <a href="javascript:void(0)" class="btn btn-default">
                                                {{ __("admin::$type.chose_avatar") }}
                                                <input type='file' onchange="imagePreview(this);"
                                                       name="img_file" class="input_file_img"/>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" id="tab-2" class="tab-pane">
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <div class="form-group @error('name') has-error @enderror">
                                            {!! Form::label('name', __("admin::$type.name"), array('class' => ' required')) !!}
                                            {!! Form::text('name', null, array('class' => 'form-control',
                                            'placeholder'=> __("admin::$type.name"),'required' => true,
                                            'minlength'=>$constant::VALIDATE['name_min_len'],
                                            'maxlength'=>$constant::VALIDATE['name_max_len'])) !!}
                                            <span class="help-block">{{ $errors->first('name', ':message') }}</span>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <div class="form-group @error('about') has-error @enderror">
                                            {!! Form::label('about', __("admin::$type.about")) !!}
                                            {!! Form::text('about', @$doc->about, array('class' => 'form-control','placeholder'=> __("admin::$type.about"))) !!}
                                            <span class="help-block">{{ $errors->first('about', ':message') }}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <div class="form-group @error('phone') has-error @enderror">
                                            {!! Form::label('phone', __("admin::$type.phone")) !!}
                                            {!! Form::text('phone', null, array('class' => 'form-control','placeholder'=> __("admin::$type.phone"))) !!}
                                            <span class="help-block">{{ $errors->first('phone', ':message') }}</span>
                                        </div>
                                    </div>

                                    <div class="col-12 col-md-6">
                                        <div class="form-group @error('email') has-error @enderror">
                                            {!! Form::label('email', __("admin::$type.email")) !!}
                                            {!! Form::text('email', null, array('class' => 'form-control','placeholder'=> __("admin::$type.email"))) !!}
                                            <span class="help-block">{{ $errors->first('email', ':message') }}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <div class="form-group @error('language_code') has-error @enderror">
                                            {!! Form::label('language_code', __("admin::$type.lang"),array('class' => ' required')) !!}
                                            {!! Form::select('language_code', $constant::LANG,null, array('class' => 'form-control')) !!}
                                            <span class="help-block">{{ $errors->first('language_code', ':message') }}</span>
                                        </div>
                                    </div>

                                    <div class="col-12 col-md-6">
                                        <div class="form-group @error('birthday') has-error @enderror">
                                            {!! Form::label('birthday', __("admin::$type.birthday")) !!}
                                            <div class="input-group date">
                                                <span class="input-group-addon">
                                                    <i class="fa fa-calendar"></i></span>
                                                {!! Form::text('birthday', @$doc->birthday, array('class' => 'form-control datepicker_decade_view','autocomplete'=>'off','placeholder'=> __("admin::$type.birthday"))) !!}
                                            </div>
                                            <span class="help-block">{{ $errors->first('birthday', ':message') }}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group @error('address') has-error @enderror">
                                            {!! Form::label('address', __("admin::$type.address")) !!}
                                            {!! Form::text('address', @$doc->info->address, array('class' => 'form-control','placeholder'=> __("admin::$type.address"))) !!}
                                            <span class="help-block">{{ $errors->first('address', ':message') }}</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group @error('gender_id') has-error @enderror">
                                            {!! Form::label('gender_id', __("admin::$type.gender")) !!}
                                            <br>
                                            {{__("admin::$type.gender0")}}
                                            <label class="switch">
                                                {!! Form::checkbox('gender_id', null, @$doc->gender_id==3,array('id' => 'gender_id')) !!}
                                                <span class="slider round"></span>
                                            </label>
                                            {{__("admin::$type.gender1")}}
                                            <br>
                                            <span class="help-block">{{ $errors->first('gender', ':message') }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="ibox-footer">
                @include('admin::common._button_submit')
            </div>
        </div>
    </div>
</div>

@push('css')
    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 50px;
            height: 24px;
        }

        /* Hide default HTML checkbox */
        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }

        /* The slider */
        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }

        .slider:before {
            position: absolute;
            content: "";
            height: 20px;
            width: 20px;
            left: 2px;
            bottom: 2px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }

        input:checked + .slider {
            background-color: #2196F3;
        }

        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }

        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }

        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }

        .slider.round:before {
            border-radius: 50%;
        }
    </style>
@endpush

@push('css')
    <link href="{{asset_admin('plugins/iCheck/custom.css')}}" rel="stylesheet">
    <link href="{{asset_admin('plugins/datapicker/datepicker3.css')}}" rel="stylesheet">
@endpush

@push('js')
    <!-- iCheck -->
    <script src="{{asset_admin('plugins/iCheck/icheck.min.js')}}"></script>
    <!-- Data picker -->
    <script src="{{asset_admin('plugins/datapicker/bootstrap-datepicker.js')}}"></script>
    <script>
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

        @if (isset($doc))
        if ($(".password_field").hasClass('has-error')) {
            $("#show_pass").iCheck('check');
            $(".password_field").show();
        } else {
            $(".password_field").hide();
        }
        $("#show_pass").on('ifChanged', function () {
            if ($("#show_pass").is(':checked')) {
                $(".password_field").show();
            } else {
                $(".password_field").hide();
            }
        });
        @endif

        $(function () {
            $('.date_picker').datepicker({
                format: 'dd/mm/yyyy',
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                calendarWeeks: true,
                autoclose: true
            });
            $('.datepicker_decade_view').datepicker({
                startView: 2,
                format: 'dd/mm/yyyy',
                todayBtn: "linked",
                keyboardNavigation: false,
                forceParse: false,
                autoclose: true
            });
        })
    </script>
@endpush
