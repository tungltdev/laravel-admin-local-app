<?php

namespace App\Admin\Http\Controllers;

use App\Admin\{Constants\AdminConstant as Constant,
    Http\Services\AdminService as ModelService,
    Models\Admin as Model,
    Models\Permission,
    Models\Role};
use Illuminate\Http\Request;

class AdminController extends BaseAdminController
{
    private $sv;

    public function __construct(ModelService $modelService)
    {
        $this->middleware('permission:admin_view', ['only' => ['index', 'show']]);
        $this->middleware('permission:admin_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:admin_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:admin_destroy', ['only' => ['destroy']]);

        $this->sv = $modelService;
        $roles_pluck = Role::pluck('name', 'id');
        $permissions_pluck = Permission::get()->pluck('name_note', 'id');

        $constant = Constant::class;
        view()->share([
            'roles_pluck'       => $roles_pluck,
            'permissions_pluck' => $permissions_pluck,
            'constant'          => $constant,
        ]);
    }

    public function index()
    {
//        $this->authorize('admin_list');
        $title = __('admin.meta_title.index');
        return view('admin::admin.index', compact('title'));
    }

    public function grid(Request $request)
    {
//        $this->authorize('list-admin');
        $limit = $request->limit;
        $s = getSortAdmin($request);
        $docs = Model::with('roles', 'permissions')->orderBy($s[0], $s[1]);
        if ($request->search) {
            $docs = $docs->where(function ($query) use ($request) {
                $query->where('name', 'like', '%' . $request->search . '%')
                    ->orWhere('username', 'like', '%' . $request->search . '%');
            });
        }
        if ($request->role_id) {
            $docs = $docs->whereHas('roles', function ($q) use ($request) {
                $q->where('id', (int)$request->role_id);
            });
        }
        if (isset($request->active)) {
            $docs = $docs->where('active', (int)$request->active);
        }
        $docs = $docs->paginate($limit);

        return view('admin::admin.grid', compact('docs'));
    }

    public function create()
    {
        $title = __("admin.new");
        return view('admin::layouts.create', compact('title'));
    }

    public function edit(Model $admin)
    {
//        $this->authorize('update', $admin);
        $doc = $admin;
        $title = __("admin.edit") . " ( $doc->username )";
        return view('admin::layouts.create', compact('title', 'doc'));
    }

    public function store(Request $request)
    {
        $table = Constant::TABLE;
        $name_min_len = Constant::VALIDATE['name_min_len'];
        $name_max_len = Constant::VALIDATE['name_max_len'];
        $username_min_len = Constant::VALIDATE['username_min_len'];
        $username_max_len = Constant::VALIDATE['username_max_len'];
        $password_min_len = Constant::VALIDATE['password_min_len'];
        $password_max_len = Constant::VALIDATE['password_max_len'];

        if ($request->hasFile('img_file')) {
            $request->validate([
                'img_file' => "image",
            ]);
        }
        $request->validate([
            'name'     => "required|min:$name_min_len|max:$name_max_len",
            'username' => "required|min:$username_min_len|max:$username_max_len|unique:$table",
            'password' => "required|string|confirmed|min:$password_min_len|max:$password_max_len",
        ]);

        $request->merge([
            'password' => bcrypt($request->password),
        ]);

        $this->sv->create($request);
        flash()->success(__('message.success.create'));
        return redirect_route_admin('admin.index');
    }

    public function update(Request $request, Model $admin)
    {
//        $this->authorize('update', $admin);
        $doc = $admin;
        $table = Constant::TABLE;
        $name_min_len = Constant::VALIDATE['name_min_len'];
        $name_max_len = Constant::VALIDATE['name_max_len'];
        $username_min_len = Constant::VALIDATE['username_min_len'];
        $username_max_len = Constant::VALIDATE['username_max_len'];


        $request->merge([
            'active' => @$request->active ? 1 : 0,
        ]);

        $request->validate([
            'name'     => "required|min:$name_min_len|max:$name_max_len",
            'username' => "required|min:$username_min_len|max:$username_max_len|unique:$table,username,$doc->id",
        ]);

        if (!empty($request->show_pass)) {
            $password_min_len = Constant::VALIDATE['password_min_len'];
            $password_max_len = Constant::VALIDATE['password_max_len'];
            $request->validate([
                'password' => "required|string|confirmed|min:$password_min_len|max:$password_max_len",
            ]);
            $request->merge([
                'password' => bcrypt($request->password),
            ]);
        } else {
            $request->merge(['password' => $doc->password]);
        }

        $this->sv->update($request, $doc);
        flash()->success(__('message.success.update'));
        return redirect_route_admin('admin.index');
    }

    public function destroy(Model $admin)
    {
        return $this->sv->destroy($admin);
    }
}
