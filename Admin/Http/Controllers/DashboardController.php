<?php

namespace App\Admin\Http\Controllers;

use App\Admin\Models\User;
use Illuminate\Http\Request;

class DashboardController extends BaseAdminController
{
    public function index()
    {
        return view('admin::dashboard.index');
    }

    public function accountData(Request $request)
    {
        $time_data = getStartEndTime($request);
        $time = $time_data['time'];
        $start = $time_data['start'];
        $end = $time_data['end'];
//        $data[__('dashboard.time')] = $data[__('dashboard.user')] = $data[__('dashboard.admin')] = [];
        $data[__('dashboard.time')] = $data[__('dashboard.user')] = [];
        $data[__('dashboard.nhatuyendung')] = $data[__('dashboard.ungvien')] = [];

        $users = User::whereBetween('created_at', [$start, $end])
            ->get();

//        $admins = Admin::whereBetween('created_at', [$start, $end])
//            ->Active()
//            ->get();

        if (@count($users)) {
            foreach ($users as $key => $value) {
                $carbon = $value->created_at;
                switch ($time) {
                    case 'day':
                        $date = $carbon->format('Y-m-d');
                        break;
                    case 'month':
                        $date = $carbon->endOfMonth()->format('Y-m-d');
                        break;
                    default:
                        $date = $carbon->endOfWeek()->format('Y-m-d');
                        break;
                }
                if (isset($data[__('dashboard.user')][$date])) {
                    $data[__('dashboard.user')][$date] += 1;
                } else {
                    $data[__('dashboard.user')][$date] = 1;
                    $data[__('dashboard.time')][$date] = $date;
                }

                if ($value->user_type_id == 1) {
                    if (isset($data[__('dashboard.nhatuyendung')][$date])) {
                        $data[__('dashboard.nhatuyendung')][$date] += 1;
                    } else {
                        $data[__('dashboard.nhatuyendung')][$date] = 1;
                        $data[__('dashboard.time')][$date] = $date;
                    }
                } else {
                    if ($value->user_type_id == 2) {
                        if (isset($data[__('dashboard.ungvien')][$date])) {
                            $data[__('dashboard.ungvien')][$date] += 1;
                        } else {
                            $data[__('dashboard.ungvien')][$date] = 1;
                            $data[__('dashboard.time')][$date] = $date;
                        }
                    }
                }
            }
        }

//        if (@count($admins)) {
//            foreach ($admins as $key => $value) {
//                $carbon = $value->created_at;
//                switch ($time) {
//                    case 'day':
//                        $date = $carbon->format('Y-m-d');
//                        break;
//                    case 'month':
//                        $date = $carbon->endOfMonth()->format('Y-m-d');
//                        break;
//                    default:
//                        $date = $carbon->endOfWeek()->format('Y-m-d');
//                        break;
//                }
//                if (isset($data[__('dashboard.admin')][$date])) {
//                    $data[__('dashboard.admin')][$date] += 1;
//                } else {
//                    $data[__('dashboard.admin')][$date] = 1;
//                    $data[__('dashboard.time')][$date] = $date;
//                }
//            }
//        }
        ksort($data[__('dashboard.time')]);
        // fix lỗi 2 mảng k bằng số lượng phần tử khiến js biểu đồ lỗi;
        if (@count($data[__('dashboard.time')])) {
            foreach ($data[__('dashboard.time')] as $value) {
                if (!isset($data[__('dashboard.user')][$value])) {
                    $data[__('dashboard.user')][$value] = 0;
                }
//                if (!isset($data[__('dashboard.admin')][$value])) {
//                    $data[__('dashboard.admin')][$value] = 0;
//                }
                if (!isset($data[__('dashboard.ungvien')][$value])) {
                    $data[__('dashboard.ungvien')][$value] = 0;
                }
                if (!isset($data[__('dashboard.nhatuyendung')][$value])) {
                    $data[__('dashboard.nhatuyendung')][$value] = 0;
                }
            }
        }

        ksort($data[__('dashboard.nhatuyendung')]);
        ksort($data[__('dashboard.ungvien')]);
        ksort($data[__('dashboard.user')]);
//        ksort($data[__('dashboard.admin')]);


//        && empty($data[__('dashboard.admin')][$key])
        foreach ($data[__('dashboard.time')] as $key => $value) {
            if (empty($data[__('dashboard.user')][$key])
                && empty($data[__('dashboard.nhatuyendung')][$key]) && empty($data[__('dashboard.ungvien')][$key])) {
                unset($data[__('dashboard.time')][$key]);
                unset($data[__('dashboard.user')][$key]);
                unset($data[__('dashboard.nhatuyendung')][$key]);
                unset($data[__('dashboard.ungvien')][$key]);
//                unset($data[__('dashboard.admin')][$key]);
            }
        }

        $data[__('dashboard.time')] = array_values($data[__('dashboard.time')]);
        $data[__('dashboard.user')] = array_values($data[__('dashboard.user')]);
        $data[__('dashboard.ungvien')] = array_values($data[__('dashboard.ungvien')]);
        $data[__('dashboard.nhatuyendung')] = array_values($data[__('dashboard.nhatuyendung')]);
//        $data[__('dashboard.admin')] = array_values($data[__('dashboard.admin')]);

        return response()->json($data);
    }
}
