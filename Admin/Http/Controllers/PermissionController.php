<?php

namespace App\Admin\Http\Controllers;

use App\Admin\{Constants\PermissionConstant as Constant,
    Http\Services\PermissionService as ModelService,
    Models\Permission as Model};

use Illuminate\Http\Request;

class PermissionController extends BaseAdminController
{
    private $sv;

    public function __construct(ModelService $modelService)
    {
        $this->middleware('permission:permission_view', ['only' => ['index', 'show']]);
        $this->middleware('permission:permission_edit', ['only' => ['edit', 'update']]);
        $this->sv = $modelService;

        $constant = Constant::class;
        view()->share([
            'constant' => $constant,
        ]);
    }

    public function index()
    {
        $title = __('permission.meta_title.index');
        return view('admin::permission.index', compact('title'));
    }

    public function grid(Request $request)
    {
        $limit = $request->limit;
        $s = getSortAdmin($request);
        $docs = Model::with('roles')->with('users')->orderBy($s[0], $s[1]);
        if ($request->search) {
            $docs = $docs->where(function ($query) use ($request) {
                $query->where('name', 'like', '%' . $request->search . '%');
            });
        }
        $docs = $docs->paginate($limit);
        return view('admin::permission.grid', compact('docs'));
    }


    public function edit(Model $permission)
    {
        $doc = $permission;
        $title = __("permission.edit") . " ( $doc->name )";
        return view('admin::layouts.create', compact('title', 'doc'));
    }

    public function update(Request $request, Model $permission)
    {
        $doc = $permission;
        $notes_min_len = Constant::VALIDATE['notes_min_len'];
        $notes_max_len = Constant::VALIDATE['notes_max_len'];

        $request->validate([
            'notes' => ["min:$notes_min_len", "max:$notes_max_len"],
        ]);

        $this->sv->update($request, $doc);
        flash()->success(__('message.success.update'));
        return redirect_route_admin('permission.index');
    }
}
