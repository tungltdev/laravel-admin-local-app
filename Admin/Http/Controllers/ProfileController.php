<?php

namespace App\Admin\Http\Controllers;

use App\Admin\Constants\AdminConstant as Constant;
use Illuminate\Http\Request;

class ProfileController extends BaseAdminController
{

    public function index()
    {
        return view('admin::profile.index', ['doc' => auth('admin')->user()]);
    }

    public function update(Request $request)
    {
        $doc = auth('admin')->user();

        $table = Constant::TABLE;
        $name_min_len = Constant::VALIDATE['name_min_len'];
        $name_max_len = Constant::VALIDATE['name_max_len'];
        $username_min_len = Constant::VALIDATE['username_min_len'];
        $username_max_len = Constant::VALIDATE['username_max_len'];


        $request->validate([
            'name'     => "required|min:$name_min_len|max:$name_max_len",
            'username' => "required|min:$username_min_len|max:$username_max_len|unique:$table,username,$doc->id",
        ]);


        if (!empty($request->show_pass)) {
            $password_min_len = Constant::VALIDATE['password_min_len'];
            $password_max_len = Constant::VALIDATE['password_max_len'];
            $request->validate([
                'password' => "required|string|confirmed|min:$password_min_len|max:$password_max_len",
            ]);
            $request->merge([
                'password' => bcrypt($request->password),
            ]);
        } else {
            $request->merge(['password' => $doc->password]);
        }

        if ($request->hasFile('img_file')) {
            $request->validate([
                'img_file' => "image",
            ]);
            $img = uploadStoragePathAdmin($request, "avatar/admin/$doc->id");
            $request->merge(['image' => $img]);
        }

        $doc->update($request->except('show_pass', 'img_file', 'roles', 'permissions'));
        flash()->success(__('message.success.update'));
        return redirect_route_admin('profile');
    }

    public function refresh()
    {
        try {
            cache()->forget(config('permission.cache.key'));
        } catch (\Exception $e) {
        }
        return 'Ping Pong ^^';
    }
}
