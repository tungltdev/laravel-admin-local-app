<?php

namespace App\Admin\Http\Controllers;

use App\Admin\Constants\PermissionConstant;
use App\Admin\Models\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class SystemController extends BaseAdminController
{
    public function __construct()
    {
        $this->middleware('permission:system_view', ['only' => ['info', 'refreshPermission']]);
        $this->middleware('permission:system_setting', ['only' => ['setting', 'saveContentEnvFile']]);
        $this->middleware('permission:system_fixStorageLink', ['only' => ['fixStorageLink']]);
        $this->middleware('permission:system_xoaCacheSessions', ['only' => ['xoaCacheSessions']]);
        $this->middleware('permission:system_xoaCacheConfig', ['only' => ['xoaCacheConfig']]);
        $this->middleware('permission:system_xoaCacheView', ['only' => ['xoaCacheView']]);
    }

    public function refreshPermission()
    {
        try {
            cache()->forget(config('permission.cache.key'));
            $permissions_defaultPermissions = PermissionConstant::LIST_PERMISSIONS;
            foreach ($permissions_defaultPermissions as $perms) {
                Permission::firstOrCreate(['name' => $perms, 'guard_name' => 'admin']);
            }
            flash()->success('Cập nhập danh sách quyền thành công');
        } catch (\Exception $e) {
            dd($e);
        }

        return redirect()->back();
    }

    public function setting()
    {
        return view('admin::system.setting');
    }

    public function info()
    {
        $cp = Cache::remember('file.composer.lock', 360000, function () {
            $cp = @json_decode(file_get_contents(base_path('composer.lock')), true);

            return $cp;
        });
        $data_more = [];
        $chmod_storage_path = getChmod(storage_path(''));
        $perms_storage_path = getPerms(storage_path(''));
        $chmod_public_path = getChmod(public_path(''));
        $perms_public_path = getPerms(public_path(''));
        $data_more['chmod_storage_path'] = $chmod_storage_path;
        $data_more['perms_storage_path'] = $perms_storage_path;
        $data_more['chmod_public_path'] = $chmod_public_path;
        $data_more['perms_public_path'] = $perms_public_path;

        return view('admin::system.info', compact('cp', 'data_more'));
    }

    public function runCmd()
    {
        $rs = request()->get('cmd');
        if (!$rs) {
            abort(503, "Vui lòng nhập lệnh");
        }
        if (request()->get('type') == 'cmd') {
            view()->share('passthru_cmd', $rs);
        } else {
            \Illuminate\Support\Facades\Artisan::call($rs);
            view()->share('artisan_cmd', \Illuminate\Support\Facades\Artisan::output());
        }
        return $this->info();
    }

    public function fixStorageLink()
    {
        try {
            @rmdir(public_path('storage'));
            @rmdir(public_path('uploads'));
            app()->make('files')->link(storage_path('app/public'), public_path('storage'));
            app()->make('files')->link(storage_path('app/uploads'), public_path('uploads'));
            flash()->success('Fix Storage Link thành công');
        } catch (\Exception $e) {

        }

        return redirect()->back();
    }

    public function xoaCacheSessions()
    {
        array_map('unlink', array_filter((array)glob(storage_path('framework/sessions/*'))));
        flash()->success('Xóa Sessions thành công');

        return redirect()->back();
    }

    public function xoaCacheConfig()
    {
        \Illuminate\Support\Facades\Artisan::call('cache:clear');
        \Illuminate\Support\Facades\Artisan::call('config:clear');
        flash()->success('Xóa Cache thành công');
        view()->share('artisan_cmd', \Illuminate\Support\Facades\Artisan::output());

        return $this->info();
    }

    public function xoaCacheView()
    {
        \Illuminate\Support\Facades\Artisan::call('view:clear');
        flash()->success('Xóa Cache View thành công');
        view()->share('artisan_cmd', \Illuminate\Support\Facades\Artisan::output());

        return $this->info();
    }

    public function refreshCache($cache_name)
    {
        try {
            cache()->forget($cache_name);
        } catch (\Exception $e) {
        }
        flash()->success('Xóa cache thành công');
        $back = in_array($cache_name, ['analyticsData_data', 'admin.dashboard.dashboard']);
        if ($back) {
            return redirect()->back();
        }

        return redirect_route_admin('system.info');
    }

    public function saveContentEnvFile(Request $request)
    {
        $request->validate([
            'content_env' => ["required", "string"],
        ]);
        file_put_contents(base_path('.env'), $request->content_env);
        \Illuminate\Support\Facades\Artisan::call('config:clear');
        flash()->success('Lưu file .env thành công');

        return redirect_route_admin('system.setting');
    }
}
