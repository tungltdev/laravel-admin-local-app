<?php

namespace App\Admin\Http\Controllers;

use App\Admin\{Constants\UserConstant as Constant,
    Http\Services\UserService as ModelService,
    Models\User as Model};
use Illuminate\Http\Request;

class UserController extends BaseAdminController
{
    private $sv;

    public function __construct(ModelService $modelService)
    {
        $this->middleware('permission:user_view', ['only' => ['index', 'show']]);
        $this->middleware('permission:user_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:user_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:user_destroy', ['only' => ['destroy']]);

        $this->sv = $modelService;
        $constant = Constant::class;
        view()->share([
            'constant' => $constant,
        ]);
    }

    public function index()
    {
        $title = __('user.meta_title.index');
        return view('admin::user.index', compact('title'));
    }

    public function grid(Request $request)
    {
        $limit = $request->limit;
        $s = getSortAdmin($request);
        $docs = Model::orderBy($s[0], $s[1]);
        if ($request->id) {
            $docs = $docs->where('id', $request->id);
        } else {
            if ($request->search) {
                $docs = $docs->where(function ($query) use ($request) {
                    $query->where('name', 'like', '%' . $request->search . '%')
                        ->orWhere('phone', 'like', '%' . $request->search . '%')
                        ->orWhere('email', 'like', '%' . $request->search . '%');
                });
            }
        }
        if (isset($request->user_type_id)) {
            $docs = $docs->where('user_type_id', (int)$request->user_type_id);
        }
        if ($request->start && $request->end) {
            $data = getStartEndTime($request);
            $docs = $docs->whereBetween('created_at', [$data['start'], $data['end']]);
        }

        $docs = $docs->paginate($limit);
        $total = $docs->total();
        return view('admin::user.grid', compact('docs', 'total'));
    }

    public function create()
    {
        $title = __("user.new");
        return view('admin::layouts.create', compact('title'));
    }

    public function edit(Model $user)
    {
        $doc = $user;
        $title = __("user.edit") . " ( $doc->name )";
        return view('admin::layouts.create', compact('title', 'doc'));
    }

    public function store(Request $request)
    {
        $table = Constant::TABLE;
        $name_min_len = Constant::VALIDATE['name_min_len'];
        $name_max_len = Constant::VALIDATE['name_max_len'];
        $username_min_len = Constant::VALIDATE['username_min_len'];
        $username_max_len = Constant::VALIDATE['username_max_len'];
        $password_min_len = Constant::VALIDATE['password_min_len'];
        $password_max_len = Constant::VALIDATE['password_max_len'];

        $request->merge([
            'gender' => @$request->gender ? 1 : 0,
        ]);
        $request->validate([
            'name'     => "required|min:$name_min_len|max:$name_max_len",
            'username' => "required|min:$username_min_len|max:$username_max_len|unique:$table",
            'password' => "required|string|confirmed|min:$password_min_len|max:$password_max_len",
        ]);

        $this->sv->create($request);
        flash()->success(__('message.success.create'));
        return redirect_route_admin('user.index');
    }


    public function update(Request $request, Model $user)
    {
        $doc = $user;
        $table = Constant::TABLE;
        $name_min_len = Constant::VALIDATE['name_min_len'];
        $name_max_len = Constant::VALIDATE['name_max_len'];
        $username_min_len = Constant::VALIDATE['username_min_len'];
        $username_max_len = Constant::VALIDATE['username_max_len'];

        $request->merge([
            'gender_id' => @$request->gender_id ? 3 : 4,
            'blocked'   => @$request->blocked ? 1 : 0,
        ]);

        $request->validate([
            'name'      => "required|min:$name_min_len|max:$name_max_len",
            'gender_id' => ["required", "in:3,4"],
            'username'  => "required|min:$username_min_len|max:$username_max_len|unique:$table,username,$doc->id",
        ]);

        if (!empty($request->show_pass)) {
            $password_min_len = Constant::VALIDATE['password_min_len'];
            $password_max_len = Constant::VALIDATE['password_max_len'];
            $request->validate([
                'password' => "required|string|confirmed|min:$password_min_len|max:$password_max_len",
            ]);
            $request->merge([
                'password' => bcrypt($request->password),
            ]);
        } else {
            $request->merge(['password' => $doc->password]);
        }

        $this->sv->update($request, $doc, ['password_confirmation', 'img_file']);
        flash()->success(__('message.success.update'));
        return redirect_route_admin('user.index');
    }

    public function destroy(Model $user)
    {
        if ($user->user_type_id == 2) {
            $email_current = $user->email;
            $user->blocked = 1;
            $user->is_show = 0;
            $user->email = "deleted_" . $email_current;
            $user->save();
            return $this->sv->destroy($user);
        }
    }
}
