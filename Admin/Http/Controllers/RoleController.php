<?php

namespace App\Admin\Http\Controllers;

use App\Admin\{Constants\RoleConstant as Constant,
    Http\Services\RoleService as ModelService,
    Models\Role as Model,
    Models\Permission};

use Illuminate\Http\Request;

class RoleController extends BaseAdminController
{
    private $sv;

    public function __construct(ModelService $modelService)
    {
        $this->middleware('permission:role_view', ['only' => ['index', 'show']]);
        $this->middleware('permission:role_create', ['only' => ['create', 'store']]);
        $this->middleware('permission:role_edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:role_destroy', ['only' => ['destroy']]);

        $constant = Constant::class;
        $this->sv = $modelService;
        $permissions_pluck = Permission::guardName('admin')->get()->pluck('name_note', 'id');
        view()->share([
            'permissions_pluck' => $permissions_pluck,
            'constant'          => $constant,
        ]);
    }

    public function index()
    {
        $title = __('role.meta_title.index');
        return view('admin::role.index', compact('title'));
    }

    public function grid(Request $request)
    {
        $limit = $request->limit;
        $s = getSortAdmin($request);
        $docs = Model::with('users:username')->withCount('users')->withCount('permissions')->orderBy($s[0], $s[1]);
        if ($request->search) {
            $docs = $docs->where(function ($query) use ($request) {
                $query->where('name', 'like', '%' . $request->search . '%');
            });
        }
        $docs = $docs->paginate($limit);
        return view('admin::role.grid', compact('docs'));
    }

    public function create()
    {
        $title = __("role.new");
        return view('admin::layouts.create', compact('title'));
    }

    public function edit(Model $role)
    {
        $doc = $role->load('permissions');
        $title = __("role.edit") . " ( $doc->name )";
        return view('admin::layouts.create', compact('title', 'doc'));
    }

    public function store(Request $request)
    {
        $table = Constant::TABLE;
        $name_min_len = Constant::VALIDATE['name_min_len'];
        $name_max_len = Constant::VALIDATE['name_max_len'];

        $request->validate([
            'name'           => "required|min:$name_min_len|max:$name_max_len|unique:$table",
            'guard_name'     => "in:admin",
            'permission_ids' => "required",
        ]);
        $this->sv->create($request, ['permission_ids']);
        flash()->success(__('message.success.create'));
        return redirect_route_admin('role.index');
    }


    public function update(Request $request, Model $role)
    {
        $doc = $role;
        $table = Constant::TABLE;
        $name_min_len = Constant::VALIDATE['name_min_len'];
        $name_max_len = Constant::VALIDATE['name_max_len'];

        $request->validate([
            'name'           => ["required", "min:$name_min_len", "max:$name_max_len", "unique:$table,name,$doc->id"],
            'guard_name'     => "in:admin",
            'permission_ids' => "required",
        ]);

        $this->sv->update($request, $doc, ['permission_ids']);
        flash()->success(__('message.success.update'));
        return redirect_route_admin('role.index');
    }

    public function destroy(Model $role)
    {
        return $this->sv->destroy($role);
    }
}
