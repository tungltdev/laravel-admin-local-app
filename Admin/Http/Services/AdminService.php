<?php

namespace App\Admin\Http\Services;

use App\Admin\Models\Admin as Model;
use Illuminate\Http\Request;

class AdminService
{

    public function create(Request $request, $except = [])
    {
        $doc = Model::create($request->except($except));
        return $doc;
    }

    public function update(Request $request, Model $doc, $except = [])
    {
        if ($request->hasFile('img_file')) {
            $request->validate([
                'img_file' => "image",
            ]);
            $img = uploadStoragePathAdmin($request, "avatar/admin/$doc->id");
            $request->merge(['image' => $img]);
        }

        $doc->update($request->except($except));

        $doc->roles()->detach();
        $doc->permissions()->detach();

        $roles_insert = [];
        if (is_array(request()->get('role_ids'))) {
            foreach (request()->get('role_ids') as $v) {
                $roles_insert[] = ['role_id' => (int)$v];
            }
        }
        $doc->roles()->sync($roles_insert);


        $permissions_insert = [];
        if (is_array(request()->get('permission_ids'))) {
            foreach (request()->get('permission_ids') as $v) {
                $permissions_insert[] = ['permission_id' => (int)$v];
            }
        }
        $doc->permissions()->sync($permissions_insert);

        return $doc;
    }

    public function destroy(Model $doc)
    {
        return response()->json(['ok' => (int)$doc->delete()]);
    }
}
