<?php

namespace App\Admin\Http\Services;

use App\Admin\Models\Permission as Model;
use Illuminate\Http\Request;

class PermissionService extends BaseService
{

    public function create(Request $request, $except = [])
    {
        $doc = Model::create($request->except($except));
        return $doc;
    }

    public function update(Request $request, Model $doc)
    {
        $doc->update($request->only('notes'));
        return $doc;
    }

    public function destroy(Model $doc)
    {
        $doc = $doc->loadCount('users')->loadCount('permissions');
        $ok = $doc->users_count + $doc->permissions_count;
        if (!$ok) {
            try {
                $ok = $doc->delete();
            } catch (\Exception $e) {
                $ok = 0;
            }
            return $this->jsonApi($ok);
        } else {
            return $this->jsonApi(0);
        }
    }
}
