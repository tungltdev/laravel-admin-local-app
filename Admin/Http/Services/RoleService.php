<?php

namespace App\Admin\Http\Services;

use App\Admin\Models\Role as Model;
use Illuminate\Http\Request;

class RoleService
{

    public function create(Request $request, $except = [])
    {
        $doc = Model::create($request->except($except));
        return $doc;
    }

    public function update(Request $request, Model $doc, $except = [])
    {
        $doc->update($request->except($except));
        return $doc;
    }

    public function destroy(Model $doc)
    {
        $doc = $doc->loadCount('users')->loadCount('permissions');
        $count = $doc->users_count + $doc->permissions_count;
        if (!$count) {
            return response()->json(['ok' => (int)$doc->delete()]);
        } else {
            return response()->json(['ok' => 0]);
        }
    }
}
