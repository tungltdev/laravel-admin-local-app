<?php

namespace TLt\Admin\Http\Requests;

use TLt\Admin\Constants as Constant;

class PostRequest extends BaseRequest
{
    protected function getValidatorInstance()
    {
        $input = [];

        $input['hidden'] = (bool)$this->input('hidden') ? 1 : 0;
        $input['publish'] = (bool)$this->input('publish') ? 1 : 0;

        request()->merge($input);
        $this->merge($input);
        return parent::getValidatorInstance();
    }

    public function rules()
    {
        switch ($this->method()) {
            case "POST":
                $content_min_len = Constant::VALIDATE['content_min_len'];
                $content_max_len = Constant::VALIDATE['content_max_len'];

                $rules = [
                    'content' => ["required", "min:$content_min_len", "max:$content_max_len"],
                    //                    'category_id' => ["required"],
                ];
                break;
            case "PUT":
                $content_min_len = Constant::VALIDATE['content_min_len'];
                $content_max_len = Constant::VALIDATE['content_max_len'];

                $rules = [
                    'content' => ["required", "min:$content_min_len", "max:$content_max_len"],
                    //                    'category_id' => ["required"],
                ];
                break;
            default:
                break;
        }

        return $rules;
    }
}
