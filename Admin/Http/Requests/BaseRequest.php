<?php

namespace TLt\Admin\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;

class BaseRequest extends FormRequest
{
    protected function getValidatorInstance()
    {
        $input = [];

        request()->merge($input);
        $this->merge($input);
        return parent::getValidatorInstance();
    }

    protected function failedValidation(Validator $validator)
    {
        if ($this->ajax() || $this->wantsJson() || $this->segment(1) == 'api') {
            // Get Errors
            $errors = (new ValidationException($validator))->errors();

            // Get Json
            $json = [
                'success' => false,
                'message' => __('An error occurred while validating the data.'),
                'data'    => $errors,
            ];


            throw new HttpResponseException(response()->json($json, JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
        }

        parent::failedValidation($validator);
    }
}
