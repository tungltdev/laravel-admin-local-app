<?php

namespace App\Admin\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as BaseAuthenticate;

class Authenticate extends BaseAuthenticate
{

    public function handle($request, Closure $next)
    {
        $this->authenticate($request, ['admin']);
        return $next($request);
    }

    protected function redirectTo($request)
    {
        return route('admin.getLogin');

//        if (!$request->expectsJson()) {
//
//        }else{
//
//        }
    }
}
