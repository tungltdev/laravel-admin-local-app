<?php

namespace App\Admin\Http\Middleware;

use Closure;

class BeforeAdmin
{
    public function handle($request, Closure $next)
    {
        try {
//            $grid = session("grid");
//            if (empty($grid)) {
//            }

            $grid = [];
            $action2 = app('request')->route()->getAction();
            $controller = class_basename($action2['controller']);
            list($controller, $action) = explode('@', $controller);
            $cur = $controller;
            if ($request->ajax()) {
                if ($request->get('reset_grid')) {
                    @$grid[$cur] = [];
                } else {
                    if ($action == 'grid') {
                        unset($_POST['_token']);
                        @$grid[$cur] = $_POST;
                    }
                }
//                session(["grid" => $grid]);
            }

            $url_current = url()->current();
            $request->grid = (object)@$grid;
            $request->gridcur = (object)@$grid[$cur];
            if (!$request->limit) {
                $request->limit = 15;
            }
//            dd($grid);
            view()->share([
                'grid'        => @$grid[$cur],
                'controller'  => $controller,
                'action'      => $action,
                'url_current' => $url_current,
                'type'        => request()->segment(2),
                'link_edit'   => str_replace('/edit', '', $url_current),
                'link_add'    => str_replace('/create', '', $url_current),
            ]);
            return $next($request);
        } catch (\Exception $e) {
        }
        return $next($request);
    }
}
