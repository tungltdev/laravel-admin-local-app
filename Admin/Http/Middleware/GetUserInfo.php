<?php

namespace App\Admin\Http\Middleware;

use Closure;

class GetUserInfo
{
    public function handle($request, Closure $next)
    {
        $admin = auth('admin')->user();
        if ($admin) {
            app()->setLocale($admin->lang ?: 'vi');
            view()->share('admin', $admin);
        }
        return $next($request);
    }
}
