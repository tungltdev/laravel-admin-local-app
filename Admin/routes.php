<?php

Route::group([
    'namespace'  => 'App\Admin\Http\Controllers',
    'middleware' => 'web',
    'prefix'     => config('admin.route.prefix'),
    'as'         => config('admin.route.as'),
], function () {
    Route::get('auth/login', 'Auth\LoginController@showLoginForm')->name('getLogin');
    Route::post('auth/login', 'Auth\LoginController@login')->name('login');
});

Route::group([
    'namespace'  => 'App\Admin\Http\Controllers',
    'middleware' => config('admin.route.middleware'),
    'prefix'     => config('admin.route.prefix'),
    'as'         => config('admin.route.as'),
], function () {
    Route::get('', 'DashboardController@index')->name('index');
    Route::get('auth/logout', 'Auth\LoginController@logout')->name('logout');
    Route::get('logs', 'LogViewerController@index')->name('log.index');

    Route::get('profile/refresh', 'ProfileController@refresh')->name('profile.refresh');
    Route::get('profile', 'ProfileController@index')->name('profile');
    Route::post('profile', 'ProfileController@update')->name('updateProfile');

    Route::get('system/refresh_cache/{cache_name}', 'SystemController@refreshCache')->name('system.refresh.cache');
    Route::get('system/refresh_permission', 'SystemController@refreshPermission')->name('system.refresh.permission');
    Route::get('system/run_cmd', 'SystemController@info');
    Route::post('system/run_cmd', 'SystemController@runCmd')->name('system.run_cmd');
    Route::get('system/setting', 'SystemController@setting')->name('system.setting');
    Route::get('system/info', 'SystemController@info')->name('system.info');
    Route::get('system/fix_storage_link', 'SystemController@fixStorageLink')->name('system.fixStorageLink');
    Route::get('system/xoa_cache_sessions', 'SystemController@xoaCacheSessions')->name('system.xoaCacheSessions');
    Route::get('system/xoa_cache_config', 'SystemController@xoaCacheConfig')->name('system.xoaCacheConfig');
    Route::get('system/xoa_cache_view', 'SystemController@xoaCacheView')->name('system.xoaCacheView');
    Route::post('system/save-content-env-file',
        'SystemController@saveContentEnvFile')->name('system.saveContentEnvFile');

    Route::get('report/post-data', 'DashboardController@postData')->name('report.post_data');
    Route::get('report/account-statistic', 'DashboardController@accountData')->name('report.account_data');

    Route::resource('user', 'UserController');
    Route::post('user/grid', 'UserController@grid');

    Route::resource('admin', 'AdminController');
    Route::post('admin/grid', 'AdminController@grid');

    Route::resource('role', 'RoleController');
    Route::post('role/grid', 'RoleController@grid');

    Route::resource('permission', 'PermissionController')->except([
        'create',
        'store',
        'destroy',
        'show',
    ]);;
    Route::post('permission/grid', 'PermissionController@grid');


    Route::resource('category', 'CategoryController');
    Route::post('category/grid', 'CategoryController@grid');

    Route::resource('tag', 'TagController');
    Route::post('tag/grid', 'TagController@grid');

    Route::resource('job', 'JobController');
    Route::post('job/grid', 'JobController@grid');
    Route::get('job/refresh/{job}', 'JobController@refresh');

    Route::resource('city', 'CityController');
    Route::post('city/grid', 'CityController@grid');

    Route::resource('service_request', 'ServiceRequestController');
    Route::post('service_request/grid', 'ServiceRequestController@grid');

    Route::post('service_log/grid', 'ServiceLogController@grid');
    Route::post('service_log/{company}', 'ServiceLogController@store');
    Route::get('service_log', 'ServiceLogController@index')->name('service_log.index');
    Route::get('service_log/create/{company}', 'ServiceLogController@create')->name('service_log.create');
    Route::get('service_log/get_time_service/{service_request_key}', 'ServiceLogController@getTimeService')
        ->name('service_log.getTimeService');
    Route::post('service_log/cancel-service/{log}', 'ServiceLogController@cancelService');

    Route::post('service/grid', 'ServiceController@grid');
    Route::get('service', 'ServiceController@index')->name('service.index');
    Route::post('service/reserve-service/{service}', 'ServiceController@reserveService');
    Route::get('service/check-job-bye-service/{job}', 'ServiceController@checkJobByeService');

    Route::resource('service_box', 'ServiceBoxController');
    Route::post('service_box/grid', 'ServiceBoxController@grid');
    Route::post('service_box/set-job-to-service-box/{job}', 'ServiceBoxController@setJobToServiceBox');


});
