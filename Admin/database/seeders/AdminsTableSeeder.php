<?php

namespace App\Admin\database\seeders;

use Illuminate\Database\Seeder;
use App\Admin\Models\Permission;
use App\Admin\Models\Role;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Admin\Constants\PermissionConstant;

class AdminsTableSeeder extends Seeder
{

    public function run()
    {
        $userModel = config('admin.database.admin_model');
        $this->command->info("Đang xóa Admins ...");
        $userModel::truncate();
        $this->command->info('Đang thêm mới 1 bản ghi Admins ...');
        $userModel::create([
            'name'     => 'Supper Founder',
            'username' => 'admin',
            'password' => bcrypt(123456),
        ]);

        $this->command->info("Đang Reset Role và Permisstion ...");

        try {
            cache()->forget('spatie.permission.cache');
        } catch (\Exception $e) {

        }

        $tableNames = config('permission.table_names');

        Model::unguard();
        DB::table($tableNames['role_has_permissions'])->delete();
        DB::table($tableNames['model_has_roles'])->delete();
        DB::table($tableNames['model_has_permissions'])->delete();
        DB::table($tableNames['roles'])->delete();
        DB::table($tableNames['permissions'])->delete();
        Model::reguard();

        try {
            cache()->forget('spatie.permission.cache');
        } catch (\Exception $e) {

        }

        $permissions_defaultPermissions = PermissionConstant::LIST_PERMISSIONS;
        foreach ($permissions_defaultPermissions as $perms) {
            Permission::firstOrCreate(['name' => $perms, 'guard_name' => 'admin']);
        }

        $roleFounder = Role::create(['name' => 'Founder', 'guard_name' => 'admin']);
        $roleFounder->syncPermissions(Permission::all());

        $role1 = Role::create(['name' => 'Manage User', 'guard_name' => 'admin']);

        $role1->givePermissionTo('admin_view');
        $role1->givePermissionTo('admin_create');
        $role1->givePermissionTo('admin_edit');
        $role1->givePermissionTo('admin_destroy');

        $admin = $userModel::find(1);
        if ($admin) {
            $admin->assignRole('Founder');
        }

//        $admin->name = 'Supper Founder';
//        $admin->username = 'admin';
//        $admin->save();

    }
}
