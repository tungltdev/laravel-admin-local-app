<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminsTable extends Migration
{
    public function up()
    {
        Schema::defaultStringLength(191);
        Schema::create(config('admin.database.admin_table'), function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->string('username', 50)->unique()->index();
            $table->string('password', 100);

            $table->string('image')->nullable();
            $table->boolean('active')->default(1)->index();
            $table->string('lang', 4)->default('vi');
            $table->timestamp('last_login')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists(config('admin.database.admin_table'));
    }
}
