<?php

namespace App\Admin\Models;

use App\Admin\Models\Relations\UserRelation as ModelRelation;
use App\Admin\Models\MutatorAccessor\UserMutatorAccessor as ModelMutatorAccessor;

class User extends BaseModel
{

    use ModelRelation, ModelMutatorAccessor;
    protected $guarded = ['id'];

    protected $hidden = [
        'password',
        'api_token',
        'remember_token',
    ];

    protected $casts = [
        'last_login_at' => 'datetime',
    ];

}
