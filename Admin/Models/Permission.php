<?php

namespace App\Admin\Models;

use Illuminate\Support\Str;

class Permission extends \Spatie\Permission\Models\Permission
{
    protected $appends = ['name_note'];

    public function scopeGuardName($query, $guard_name)
    {
        return $query->where('guard_name', $guard_name);
    }

    public function getNameNoteAttribute()
    {
        if ($this->notes) {
            $name = "$this->name (" . Str::limit($this->notes, 40) . ")";

            return $name;
        } else {
            return $this->name;
        }
    }
}
