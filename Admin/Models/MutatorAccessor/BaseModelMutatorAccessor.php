<?php

namespace App\Admin\Models\MutatorAccessor;

trait BaseModelMutatorAccessor
{

    public function scopesActive($query)
    {
        return $query->where('active', 1);
    }

}
