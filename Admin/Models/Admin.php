<?php

namespace App\Admin\Models;

use App\Admin\Observers\AdminObserver;
use Illuminate\Foundation\Auth\User as Authenticatable;

//use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class Admin extends Authenticatable
{
//Notifiable,
    use HasRoles;
    protected $guard = 'admin';
    protected $guard_name = 'admin';

    protected $casts = [
        'last_login' => 'date',
    ];

    protected $fillable = [
        'name',
        'username',
        'password',
        'image',
        'active',
        'lang',
        'last_login',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function __construct(array $attributes = [])
    {

        $this->setTable(config('admin.database.admin_table'));

        parent::__construct($attributes);
    }

    protected static function boot()
    {
        parent::boot();

        Admin::observe(AdminObserver::class);
    }
//
//    public function getJWTIdentifier()
//    {
//        return $this->getKey();
//    }
//
//    public function getJWTCustomClaims()
//    {
//        return [];
//    }

//    public function roles()
//    {
//        return $this->morphToMany('App\Models\Admin', 'model', 'model_has_roles', 'model_id', 'role_id', 'id', 'id',
//            false);
//    }
}
