<?php

namespace App\Admin\Models;

use Illuminate\Database\Eloquent\Model;
use App\Admin\Models\MutatorAccessor\BaseModelMutatorAccessor as ModelMutatorAccessor;

class BaseModel extends Model
{

    use ModelMutatorAccessor;

}
