<?php

namespace App\Admin\Models;

use App\Admin\Observers\RoleObserver;

class Role extends \Spatie\Permission\Models\Role
{

    /*
  |--------------------------------------------------------------------------
  | FUNCTIONS
  |--------------------------------------------------------------------------
  */
    protected static function boot()
    {
        parent::boot();

        Role::observe(RoleObserver::class);
    }

//    public function permissions(): BelongsToMany
//    {
//        return $this->belongsToMany(
//            config('permission.models.permission'),
//            config('permission.table_names.role_has_permissions'),
//            'role_id',
//            'permission_id'
//        );
//    }
//
//    /**
//     * A role belongs to some users of the model associated with its guard.
//     */
//    public function users(): MorphToMany
//    {
//        return $this->morphedByMany(
//            getModelForGuard($this->attributes['guard_name']),
//            'model',
//            config('permission.table_names.model_has_roles'),
//            'role_id',
//            config('permission.column_names.model_morph_key')
//        );
//    }
//
}
