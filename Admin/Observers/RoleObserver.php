<?php

namespace App\Admin\Observers;

use App\Admin\Models\Role as Model;
use Illuminate\Support\Facades\DB;

class RoleObserver
{

    public function created(Model $role)
    {
        $permissions_insert = [];
        if (is_array(request()->get('permission_ids'))) {
            foreach (request()->get('permission_ids') as $v) {
                $permissions_insert[] = ['permission_id' => (int)$v];
            }
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        $role->syncPermissions($permissions_insert);
    }

    public function updating(Model $role)
    {
    }

    public function updated(Model $role)
    {
    }

    public function saving(Model $role)
    {
        $permissions_insert = [];
        if (is_array(request()->get('permission_ids'))) {
            foreach (request()->get('permission_ids') as $v) {
                $permissions_insert[] = ['permission_id' => (int)$v];
            }
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        $role->syncPermissions($permissions_insert);
    }

    public function deleting(Model $role)
    {

    }

    public function deleted(Model $role)
    {

    }

    public function restored(Model $role)
    {
        //
    }

    public function forceDeleted(Model $role)
    {

    }
}
