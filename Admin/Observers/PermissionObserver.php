<?php

namespace App\Admin\Observers;

use App\Admin\Models\Permission as Model;

class PermissionObserver
{

    public function created(Model $role)
    {
    }

    public function updating(Model $role)
    {
    }

    public function updated(Model $role)
    {
    }

    public function saving(Model $role)
    {

    }

    public function deleting(Model $role)
    {

    }

    public function deleted(Model $role)
    {

    }

    public function restored(Model $role)
    {
    }

    public function forceDeleted(Model $role)
    {

    }
}
