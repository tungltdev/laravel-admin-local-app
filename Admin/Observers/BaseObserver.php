<?php

namespace App\Admin\Observers;

interface BaseObserver
{
    //retrieved
    //creating
    //created
    //updating
    //updated
    //saving
    //saved
    //deleting
    //deleted
    //restoring
    //restored

    public function created();

    public function creating();

    public function updated();

    public function updating();

    public function deleted();

    public function restored();

    public function forceDeleted();
}
