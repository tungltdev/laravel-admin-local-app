<?php

namespace App\Admin\Observers;

use App\Admin\Models\Admin as Model;

class AdminObserver
{

    public function created(Model $admin)
    {
        $roles_insert = [];
        if (is_array(request()->get('role_ids'))) {
            foreach (request()->get('role_ids') as $v) {
                $roles_insert[] = ['role_id' => (int)$v];
            }
        }
        $admin->roles()->sync($roles_insert);

        $permissions_insert = [];
        if (is_array(request()->get('permission_ids'))) {
            foreach (request()->get('permission_ids') as $v) {
                $permissions_insert[] = ['permission_id' => (int)$v];
            }
        }
        $admin->permissions()->sync($permissions_insert);


        if (request()->hasFile('img_file')) {
            $img = uploadStoragePathAdmin(request(), "avatar/admin/$admin->id");;
            $admin->image = $img;
            $admin->save();
        }

    }

    public function updating(Model $admin)
    {
        if ($admin->isDirty('image')) {
            $old_avatar = $admin->getOriginal('image');
            unlinkStorage($old_avatar);
        }
    }

    public function updated()
    {
    }

    public function saving(Model $admin)
    {

    }

    public function deleting(Model $admin)
    {
        $admin->roles()->detach();
        $admin->permissions()->detach();
        unlinkStorage($admin->image);
    }

    public function deleted(Model $admin)
    {

    }

    public function restored(Model $admin)
    {
        //
    }

    public function forceDeleted(Model $admin)
    {
        $admin->roles()->detach();
        $admin->permissions()->detach();
        unlinkStorage($admin->image);
    }
}
