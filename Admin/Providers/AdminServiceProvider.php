<?php

namespace App\Admin\Providers;

use Illuminate\Support\Arr;
use Illuminate\Support\ServiceProvider;
use App\Admin\Http\Middleware\Authenticate;
use App\Admin\Http\Middleware\RedirectIfAuthenticated;
use App\Admin\Console\Commands\InstallCommand;
use App\Admin\Console\Commands\UninstallCommand;

class AdminServiceProvider extends ServiceProvider
{
    protected $commands = [
        InstallCommand::class,
        UninstallCommand::class,
    ];

    protected $routeMiddleware = [
        'auth_IN_admin'  => Authenticate::class,
        'guest_IN_admin' => RedirectIfAuthenticated::class,
        'role'           => \Spatie\Permission\Middlewares\RoleMiddleware::class,
        'permission'     => \Spatie\Permission\Middlewares\PermissionMiddleware::class,
    ];

    protected $middlewareGroups = [
        'admin' => [
            'auth_IN_admin',
            \App\Admin\Http\Middleware\BeforeAdmin::class,
            \App\Admin\Http\Middleware\GetUserInfo::class,
        ],
    ];

    public function register()
    {
        $this->loadAdminConfig();
        $this->registerRouteMiddleware();
        $this->commands($this->commands);
    }

    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'admin');
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'admin');
        if (file_exists($routes = admin_path('routes.php'))) {
            $this->loadRoutesFrom($routes);
        }
        $this->registerPublishing();

//        $this->loadRoutesFrom(__DIR__ . '/../../routes/web.php');
//        $this->loadTranslationsFrom(__DIR__ . '/../../resources/lang', 'admin');

        //        $this->mergeConfigFrom(__DIR__ . '/../../config/custom.php', 'admin');
//        $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');
//        $this->loadFactoriesFrom(__DIR__.'/../../database/factories');

    }

    protected function loadAdminConfig()
    {
        config(Arr::dot(config('admin.auth', []), 'auth.'));
    }


    protected function registerRouteMiddleware()
    {
        // register route middleware.
        foreach ($this->routeMiddleware as $key => $middleware) {
            app('router')->aliasMiddleware($key, $middleware);
        }
        // register middleware group.
        foreach ($this->middlewareGroups as $key => $middleware) {
            app('router')->middlewareGroup($key, $middleware);
        }
    }

    protected function registerPublishing()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([__DIR__ . '/../config' => config_path()], 'public');
            $this->publishes([__DIR__ . '/../public' => public_path('vendor/admin')], 'public');
            $this->publishes([__DIR__ . '/../database/migrations' => database_path('migrations')], 'public');
            $this->publishes([__DIR__ . '/../resources/views' => resource_path('views/vendor/admin')]);
            $this->publishes([__DIR__ . '/../resources/lang' => resource_path('lang/vendor/admin')], 'public');
        }
    }
}
