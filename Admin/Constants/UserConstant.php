<?php

namespace App\Admin\Constants;

class UserConstant
{
    const TYPE = 'user';
    const TABLE = 'users';

    const LANG = ['vi' => 'Việt Nam', 'en' => 'Tiếng anh'];

    const VALIDATE = [
        'name_min_len'     => 4,
        'name_max_len'     => 50,
        'username_min_len' => 4,
        'username_max_len' => 30,
        'password_min_len' => 6,
        'password_max_len' => 30,
    ];


}
