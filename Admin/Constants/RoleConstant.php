<?php

namespace App\Admin\Constants;

class RoleConstant extends Constants
{

    const TYPE = 'role';
    const TABLE = 'roles';

    const GUARD_NAME = [
        'admin' => 'admin',
    ];
    const VALIDATE = [
        'name_min_len' => 1,
        'name_max_len' => 150,
    ];


}
