<?php

namespace App\Admin\Constants;

class PermissionConstant extends Constants
{
    const TYPE = 'permission';
    const TABLE = 'permissions';


    const VALIDATE = [
        'name_min_len'  => 1,
        'name_max_len'  => 150,
        'notes_min_len' => 1,
        'notes_max_len' => 190,
    ];


    const LIST_PERMISSIONS = [
        'debugbar_view',
        'system_setting',
        'system_view',
        'system_fixStorageLink',
        'system_xoaCacheSessions',
        'system_xoaCacheConfig',
        'system_xoaCacheView',
        'log_viewer_view',

        'admin_view',
        'admin_create',
        'admin_edit',
        'admin_destroy',

        'user_view',
        'user_create',
        'user_edit',
        'user_destroy',

        'role_view',
        'role_create',
        'role_edit',
        'role_destroy',

        'permission_view',
        'permission_edit',

        'category_view',
        'category_create',
        'category_edit',
        'category_destroy',

    ];

}
