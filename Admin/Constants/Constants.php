<?php

namespace App\Admin\Constants;

class Constants
{
    const ENABLE_IMGUR = true;
    const CACHE_EXPIRATION = 10;
    const FORMAT_DATE = 'd/m/Y';
    const FORMAT_DATE_TIME = 'd/m/Y H:i:s';
    const FORMAT_DATE_TIME_SQL = 'Y-m-d H:i:s';
    const FORMAT_TIME = 'H:i:s';

}
