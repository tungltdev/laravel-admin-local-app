<?php

namespace App\Admin\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class InstallCommand extends Command
{

    protected $signature = 'admin:install';

    protected $description = 'Cài đặt admin với 1 dòng lệnh';

    protected $directory = '';

    protected $directory_name = '';

    public function handle()
    {
        $this->initDatabase();
//
//        $b1 = $this->initAdminDirectory();
//        if($b1)
    }

    public function initDatabase()
    {
        $this->call('migrate');
        $this->call('db:seed', ['--class' => \App\Admin\database\seeders\AdminsTableSeeder::class]);
    }

    protected function initAdminDirectory()
    {
        $this->directory = config('admin.directory');
        $this->directory_name = config('admin.directory');
        $this->directory_name = explode('\\', $this->directory_name);
        $this->directory_name = last($this->directory_name);

        if (is_dir($this->directory)) {
            $this->line("<error>{$this->directory} đã tồn tại, vui lòng thử lại !</error> ");
            return 0;
        }

        $this->makeDir('/');
        $this->createRoutesFile();
        $this->makeDir('Http/Controllers');
        $this->makeDir('Http/Controllers/Auth');
        $this->makeDir('Http/Middleware');
        $this->makeDir('Http/Requests');
        $this->makeDir('Http/Resources');
        $this->makeDir('Http/Services');
        $this->makeDir('Constants');
        $this->makeDir('Models');
        $this->line('<info>Đã tạo dự án admin tại :</info> ' . str_replace(base_path(), '', $this->directory));

        $dir = __DIR__;
        $base = str_replace('\app', '\\', $dir);

        $files = File::glob($base . 'Http/Controllers/*.php');
        $files2 = File::glob($base . 'Http/Controllers/Auth/*.php');
        $array_Controller = array_merge($files, $files2);

        foreach ($array_Controller as $c) {
            $this->createController($c);
        }

        $array_Services = File::glob($base . 'Http/Services/*.php');
        foreach ($array_Services as $c) {
            $this->createAuto($c, 'Http\Services');
        }

        $array_Constants = File::glob($base . 'Constants/*.php');
        foreach ($array_Constants as $c) {
            $this->createAuto($c, 'Constants');
        }

        $array_Models = File::glob($base . 'Models/*.php');
        foreach ($array_Models as $c) {
            $this->createAuto($c, 'Models');
        }
        return 1;
    }

    protected function createController($goc)
    {
        $moi = explode('\laravel-admin', $goc);
        $name = $moi[1];
        $a = $this->directory . "/$name";
        $contents = $this->laravel['files']->get($goc);

        $contents = str_replace('DummyNamespace', config('admin.route.namespace'), $contents);
        $contents = str_replace('NameSpaceBase', $this->getNameSpaceBase(), $contents);
        $this->laravel['files']->put($a, $contents);
        $this->line(str_replace(base_path(), '', $a));
    }

    protected function createAuto($goc, $getNameSpace)
    {
        $moi = explode('\laravel-admin', $goc);
        $name = $moi[1];
        $a = $this->directory . "/$name";
        $contents = $this->laravel['files']->get($goc);

        $contents = str_replace('DummyNamespace', $this->getNameSpace($getNameSpace), $contents);
        $contents = str_replace('NameSpaceBase', $this->getNameSpaceBase(), $contents);
        $this->laravel['files']->put($a, $contents);
        $this->line(str_replace(base_path(), '', $a));
    }

    protected function createRoutesFile()
    {
        $file = $this->directory . '/routes.php';
        $contents = $this->laravel['files']->get(__DIR__ . "\Routes/routes.stub");
        $this->laravel['files']->put($file, str_replace('DummyNamespace', config('admin.route.namespace'), $contents));
        $this->line('<info>File routes admin định nghĩa tại:</info> ' . str_replace(base_path(), '', $file));
    }

    protected function makeDir($path = '')
    {
        $this->laravel['files']->makeDirectory("{$this->directory}/$path", 0777, true, true);
    }

    protected function getNameSpace($path)
    {

        return $this->getNameSpaceBase() . "\\$path";
    }

    protected function getNameSpaceBase()
    {

        return "App\\" . $this->directory_name;
    }
}
