<?php

namespace App\Admin\Console\Commands;

use Illuminate\Console\Command;

class UninstallCommand extends Command
{

    protected $signature = 'admin:uninstall';

    protected $description = 'Xóa admin package';

    public function handle()
    {
        if (!$this->confirm('Are you sure to uninstall laravel-admin?')) {
            return;
        }

        $this->removeFilesAndDirectories();

        $this->line('<info>Uninstalling laravel-admin!</info>');
    }

    protected function removeFilesAndDirectories()
    {
        $this->laravel['files']->deleteDirectory(app_path('Admin'));
        $this->laravel['files']->deleteDirectory(public_path('vendor/admin/'));
        $this->laravel['files']->deleteDirectory(resource_path('views/vendor/admin'));
        $this->laravel['files']->delete(config_path('admin.php'));
    }
}
